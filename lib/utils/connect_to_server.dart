//import 'dart:core' as prefix0;
import 'dart:io';
import 'package:async/async.dart';
import 'package:flutter/foundation.dart';
import 'package:path/path.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';



Future<dynamic> postToServer({@required String url,@required Map<String,dynamic> body,@required Function executeBody}) async{

//  print('');
//  print('=============>>>>>>>>>>>> $url');
//  print('');
//  print('===================>>>.. $body');
//  print('');

//  try {
//    final result = await InternetAddress.lookup(appUrl.url);
//    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
//      print('// I am connected to a mobile or wifi network.');
//    }
//  } on SocketException catch (_) {
//    print('// I am not connected to the internet');
//  }
//
//  var connectivityResult = await (Connectivity().checkConnectivity());
//  if (connectivityResult == ConnectivityResult.mobile || connectivityResult == ConnectivityResult.wifi) {
//
//  } else{
//
//  }



  print("url ====> $url");
  print('body ===>> $body');

  return new http.Client()
      .post(url,
      headers: {'Content-type': 'application/json','X-localization':'ar'},
      body:jsonEncode(body)
  )
      .then((http.Response r) {

        String mbody = r.body.substring(1,r.body.length-2);
      var map =  json.decode(mbody);
        print('statusCode ====> ${r.statusCode}');
        print('reasonPhrase ====> ${r.reasonPhrase}');
        print('response $url ====> ${map}');
    executeBody(map);

  })
      .whenComplete(() {
//        completion();
      print('==========>>> completed');
  }).timeout(Duration(seconds: 5),onTimeout: (){
    postToServer(url: url, body: body, executeBody: executeBody);
    print("&&&&&&&&&&&&&&>>>>>>>> timeout ");
  }).catchError((error){
//    postToServer(url: url, body: body, executeBody: executeBody);
    print('%%%%%%%%%%%%%%%%%%%%%%% catch error $error');
  });

}

Future<dynamic> uploadInvoice({@required String url,@required Map<String,String> body,@required File imageFile,@required Function executeCode}) async{


  // open a bytestream
  var stream = new http.ByteStream(DelegatingStream.typed(imageFile.openRead()));
  // get file length
  var length = await imageFile.length();

  // string to uri
  var uri = Uri.parse(url);

  // create multipart request
  var request = new http.MultipartRequest("POST", uri);

  // multipart that takes file
  var multipartFile = new http.MultipartFile('file', stream, length,
      filename: basename(imageFile.path));

  // add file to multipart
  request.files.add(multipartFile);

  request.fields.addAll(body);
  // add fields to request
//  if(body != null)
//    request.fields = body;
  // send
  var response = await request.send();
  print(response.statusCode);

  // listen for response
  response.stream.transform(utf8.decoder).listen((value) {
    String mbody = value.substring(1,value.length-2);
    var map =  json.decode(mbody);
    print(map);
    executeCode(map);
  });
}