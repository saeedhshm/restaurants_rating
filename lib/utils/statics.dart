import 'package:intl/intl.dart';


int user_id = -1;


DateTime getDate(String dateTime){
  return DateTime.fromMicrosecondsSinceEpoch(int.parse(dateTime.substring(6,dateTime.length-2))  * 1000);
}
String dateTime(DateTime dateTime){
//  print(dateTime);
  DateFormat dateFormat = DateFormat("yyyy-MM-dd");
  String string = dateFormat.format(dateTime);
  return string;
}