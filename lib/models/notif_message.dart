import 'package:restu_ratingflutter_app/utils/statics.dart';
import 'package:restu_ratingflutter_app/utils/app_urls.dart';
import 'package:restu_ratingflutter_app/utils/connect_to_server.dart' as http;

class NotifMessage{
  int ClientID;
  String ClientName;
  int MssgeID;
  int RestaurantID;
  String RestaurantName;
  int Points;
  String Prize;
  String Mssge;
  DateTime Sendtime;

  NotifMessage.fromJson(Map<String,dynamic> map){
    ClientID = map['ClientID'];
    ClientName = map['ClientName'];
    MssgeID = map['MssgeID'];
    RestaurantID = map['RestaurantID'];
    RestaurantName = map['RestaurantName'];
    Points = map['Points'];
    Prize = map['Prize'];
    Mssge = map['Mssge'];
    Sendtime = getDate(map['Sendtime']);

  }
}

class NotifMessageDao{

  Future<dynamic> getNofs(int clientId, Function completion){

    String url = '$apiUrl/SendSendMssge';

    Map<String,dynamic> params = {"MssgeID" : "9", "ClientID" :clientId, "RestaurantID": "-1"};

    return http.postToServer(url: url, body: params, executeBody: (map){
      List<NotifMessage> notifs = List();
      for(var item in map){
        notifs.add(NotifMessage.fromJson(item));
      }
      completion(notifs);
//      return map;
    });
  }
}