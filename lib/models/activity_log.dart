
import 'package:restu_ratingflutter_app/generated/i18n.dart';
import 'package:restu_ratingflutter_app/utils/connect_to_server.dart' as http;
import 'package:restu_ratingflutter_app/utils/app_urls.dart';
import 'package:restu_ratingflutter_app/utils/statics.dart';

class UserActivity{
  String RestaurantName;
  String Comment;
  dynamic TotalRate;
  DateTime RateDate;
  bool Active;
  String ActiveName;

  UserActivity.fromJson(Map<String,dynamic> map){
    this.RestaurantName = map['RestaurantName'];
    this.Comment = map['Comment'];
    this.TotalRate = map['TotalRate'];
    this.RateDate = getDate(map['RateDate']);
    this.Active = map['Active'] == 1;
    this.ActiveName = map['ActiveName'];

    print('#@#@#@#@#@#@# ${RestaurantName}');
    print('#@#@#@#@#@#@# ${Comment}');
    print('#@#@#@#@#@#@# ${TotalRate}');
    print('#@#@#@#@#@#@# ${RateDate}');
    print('#@#@#@#@#@#@# ${Active}');
    print('#@#@#@#@#@#@# ${RestaurantName}');
  }
}

class UserActivityDao{

  Future<dynamic> getUserActivities({int userId,int restauId,Function completion}){

    var url = '$apiUrl/ClientRestaurantRate';
    var params = {"ClientID" : userId , "RestaurantID" : restauId};

    return http.postToServer(url: url, body: params, executeBody: (map){
      List<UserActivity> activites = List();
      for(var item in map){

        activites.add(UserActivity.fromJson(item));
      }
      completion(activites);
    });
  }

}