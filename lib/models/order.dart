
import 'package:restu_ratingflutter_app/utils/app_urls.dart';
import 'package:restu_ratingflutter_app/utils/connect_to_server.dart' as http;

class OrderItem {
  String orderStatusId = "2";
  int recipeId ;
  int count;
  String price;
  String recipeName;
  String recipeGroup;
  int restaurantId;
  String orderImage;

}

class OrderDao{

  Future<dynamic> requestOrder(int restoId,int userId,List<OrderItem> items, Function completion){

    String url = '$apiUrl/AddOrder';

    Map<String,dynamic> params = {"ClientID" : "$userId" , "RestaurantID" : "$restoId",
      "OrderDetail":
        items.map((item){
          return {"OrderStatusID":"2","RecipeID":"${item.recipeId}","Count":"${item.count}","Price":"${item.price}"};
        }).toList()
    };

    return http.postToServer(url: url, body: params, executeBody: (map){
      print(map);
      completion();
    });
  }
}