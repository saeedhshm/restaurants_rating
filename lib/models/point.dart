import 'package:restu_ratingflutter_app/utils/connect_to_server.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:restu_ratingflutter_app/utils/app_urls.dart';

class Point{
  String clientName, restauName,prize, msgs, date;
  int msgId,restuId,points;

  Point();

  Point.fromJson(Map<String,dynamic> map){
    this.clientName = map['ClientName'];
    this.msgId = map['MssgeID'];
    this.restuId = map['RestaurantID'];
    this.restauName = map['RestaurantName'];
    this.points = map['Points'];
    this.prize = map['Prize'];
    this.msgs = map['Mssge'];
    this.date = map['Sendtime'];
  }
}

class ReplacingPoints{

  Future<dynamic> replacePoints(Function completion)async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int user_id = (prefs.getInt('user_id') ?? -1);
    String url = '$apiUrl/SendSendMssge';

    Map<String,dynamic> params = {"MssgeID" : "9", "ClientID" : user_id, "RestaurantID": "-1"};

    http.postToServer(url: url, body: params, executeBody: (map){
      List<Point> points = List();
      for(var p in map){
        print('==================================  $p');
        points.add(Point.fromJson(p));
      }

      completion(points);
    });

  }
}