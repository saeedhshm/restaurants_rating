import 'package:restu_ratingflutter_app/utils/connect_to_server.dart' as http;
import 'package:restu_ratingflutter_app/utils/app_urls.dart';

class CurrentOffer{
  int OfferID;
  String OfferName;
  String OfferPic;
  DateTime SDate;
  DateTime EDate;
  int RestaurantID;
  String RestaurantName;

  CurrentOffer.fromJson(Map<String,dynamic> map){
    OfferID = map['OfferID'];
    OfferName = map['OfferName'];
    OfferPic = map['OfferPic'];
    RestaurantID = map['RestaurantID'];
    RestaurantName = map['RestaurantName'];
    this.SDate = DateTime.fromMicrosecondsSinceEpoch(int.parse(map['SDate'].substring(6,map['SDate'].length-2))  * 1000);
    if(map['EDate'] != null)
      this.EDate = DateTime.fromMicrosecondsSinceEpoch(int.parse(map['EDate'].substring(6,map['EDate'].length-2))  * 1000);
    else
    print('================== RestaurantName ${this.EDate}');
  }
}

class CurrentOfferDao{

  Future<dynamic> getCurrentOffers(Function completion){

    String url = '$apiUrl/CrntOffers';
    
    return http.postToServer(url: url, body: null, executeBody: (map){
      print(map);
      List<CurrentOffer> offers = List();
      for(var joffer in map){
        var offer = CurrentOffer.fromJson(joffer);
        offers.add(offer);
      }
      completion(offers);
    });
  }

}