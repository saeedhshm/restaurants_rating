import 'package:restu_ratingflutter_app/utils/connect_to_server.dart' as http;
import 'package:restu_ratingflutter_app/utils/app_urls.dart';

class BuyingClientRequestDao{

  Future<dynamic> sendBuyingPointsRequest(int user_id,int restau_id,Function completion){

    String url = '$apiUrl/BuyClientRequest';

    Map<String, dynamic> params = {
      "ClientID" : "$user_id" , "RestaurantID" : "$restau_id"
    };

    http.postToServer(url: url, body: params, executeBody: (params){
      completion();
    });
  }
}