import 'package:restu_ratingflutter_app/utils/app_urls.dart';

import 'package:restu_ratingflutter_app/utils/connect_to_server.dart' as http;

class RecipeGroup{
  int RecipeGroupID;
  int RestaurantID;
  String RecipeGroupName;
  String RecipeGroupPic;

  RecipeGroup.fromJson(Map<String,dynamic> map){
     this.RecipeGroupID = map['RecipeGroupID'];
     this.RestaurantID = map['RestaurantID'];
     this.RecipeGroupName = map['RecipeGroupName'];
     this.RecipeGroupPic = map['RecipeGroupPic'];

  }
}

class Recipe{
  int RecipeID;
  int RecipeGroupID;
  int RestaurantID;
  String RecipeName;
  String RecipePic;
  dynamic Price;

  Recipe.fromJson(Map<String,dynamic> map){
     this.RecipeID = map['RecipeID'];
     this.RecipeGroupID = map['RecipeGroupID'];
     this.RestaurantID = map['RestaurantID'];
     this.RecipeName = map['RecipeName'];
     this.RecipePic = map['RecipePic'];
     this.Price = map['Price'];
  }
}

class RecipeDao{

  void getRecipe(int restoId,int recipeGroupId, Function completion){

    Map<String,dynamic> map = {"RestaurantID" : restoId, "RecipeGroupID" : recipeGroupId};
    String url = '$apiUrl/Recipe';

    http.postToServer(url: url, body: map, executeBody: (map){
      var recipes = List<Recipe>();

      for(var item in map){
        recipes.add(Recipe.fromJson(item));
      }
      completion(recipes);
    });
  }

  void getRecipeGroup(int restoId,Function completion){

    Map<String,dynamic> map = {"RestaurantID" : restoId};
    String url = "$apiUrl/RecipeGroup";

    http.postToServer(url: url, body: map, executeBody: (map){
      var recipesGrop = List<RecipeGroup>();
      for (var rGroup in map){
        recipesGrop.add(RecipeGroup.fromJson(rGroup));
      }
      completion(recipesGrop);
    });

  }
}