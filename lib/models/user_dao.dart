import 'package:shared_preferences/shared_preferences.dart';

import 'user.dart';
import 'resturant.dart';

import 'package:restu_ratingflutter_app/utils/app_urls.dart';
import 'package:restu_ratingflutter_app/utils/connect_to_server.dart' as http;

class UserDao{

  final User _user;
  UserDao(this._user);


  Future<dynamic> login(Function executeCode) async{
    String url = '$apiUrl/Login';

    Map<String,dynamic> params = {"Password" : _user.password , "email" : _user.email.trim()};

    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('password', _user.password );
    await prefs.setString('email', _user.email);

    http.postToServer(url: url, body: params, executeBody: (map){
      User user;
      for(var muser in map){
        user = User.fromJson(muser);
      }


      executeCode(user);

    });
  }


  Future<dynamic> loginManager(Function executeCode) async{
    String url = '$apiUrl/ManagerLogin';

    Map<String,dynamic> params = {"MgrPassword" : _user.password , "MngUsername" : _user.email};

    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('password', _user.password );
    await prefs.setString('email', _user.email);

    http.postToServer(url: url, body: params, executeBody: (map){
      Restaurant restauManager;
      print(map);

      for(var muser in map){
        restauManager = Restaurant.fromJson(muser);
      }


      executeCode(restauManager);

    });
  }
  Future<dynamic> retrievePassword(String email,Function completion) async {
    String url = '$apiUrl/ForgetPwd';

    Map<String,dynamic> params = {"RequestData" : email};
    http.postToServer(url: url, body: params, executeBody: (map){

      print(map);
      completion(map['Msg']);

    });
  }

  Future<dynamic> replacePoints(Function completion) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int user_id = (prefs.getInt('user_id') ?? -1);
    String url = '$apiUrl/ReplaceRequest';

    Map<String,dynamic> params = {"ClientID" : user_id};
    http.postToServer(url: url, body: params, executeBody: (map){

      print(map);
      completion(map['Msg']);

    });
  }

  Future<dynamic> registerUser(Function completion){
    String url = '$apiUrl/Registration';
    Map<String,dynamic> params = {
      "ClientID" : "0" ,
      "ClientName" : _user.name,
      "Password" : _user.password,
      "email" : _user.email,
      "Mobile" : _user.mobile
    };

    http.postToServer(url: url, body: params, executeBody: (map) async {

      int msgId = map['MsgID'];
      if(msgId == 1){
        SharedPreferences prefs = await SharedPreferences.getInstance();
        int user_id = map['ClientID'];
        prefs.setInt('user_id', user_id);
      }
      completion(msgId,map['Msg']);

    });
  }

  Future<dynamic> getUserInfo(Function completion) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int user_id = (prefs.getInt('user_id') ?? -1);
    String url = '$apiUrl/ClientData';

    Map<String,dynamic> params = {"ClientID" : user_id};

    http.postToServer(url: url, body: params, executeBody: (map){
      User user;
      for(var muser in map){
        user = User.fromJson(muser);
      }
      completion(user);

    });
  }


  Future<dynamic> updateUserProfile(Function completion) async {
    String url = '$apiUrl/UpdateProfile';

    Map<String,dynamic> params = {"ClientID" : _user.userId,
      "ClientName" : _user.name,
      "Password" : _user.password ,
      "email" : _user.email,
      "Mobile" : _user.mobile};

    http.postToServer(url: url, body: params, executeBody: (map){

      print(map);
      completion(map['Msg']);

    });
  }
}