import 'dart:core';
import 'dart:io';

import 'package:restu_ratingflutter_app/utils/app_urls.dart';
import 'package:restu_ratingflutter_app/utils/app_urls.dart' as prefix0;
import 'package:restu_ratingflutter_app/utils/connect_to_server.dart' as http;

class RestoImage{
  String FileName;

  RestoImage.fromJson(Map<String,dynamic> map){
    FileName = map['FileName'];
  }
}
class Restaurant {
  int id;
  String name;
  int typeId;
  String address;
  int rate;
  String mainImg;
  double long = 0.0;
  double lat = 0.0;
  String note;
  DateTime startDate;
  DateTime endDate;
  String typeName;
  String MngUsername;
//  String MngUsername;
  dynamic lRate;
  String mobile;
  String tel;
  bool showRate;
  bool showRestau;

  Restaurant.def();

  Restaurant({this.id,this.name,this.address,this.rate,this.mainImg,this.long,this.lat,this.note});

  Restaurant.fromJson(Map<String, dynamic> map){
    print("*************************");
    print('++++++++++>>>>>>>>> ${map['RestaurantName']}');
    print('++++++++++>>>>>>>>> ${double.tryParse(map['Lat']) ?? 0.0}');

    this.id = map['RestaurantID'];
    this.name = map['RestaurantName'];
    this.typeId = map['RestaurantTypeID'];
    this.address = map['Address'];
    this.rate = map['Rate'];
    this.mainImg = map['MainPicture'];

    this.lat = double.tryParse(map['Lat']) ?? 0.0;
    this.long = double.tryParse(map['long']) ?? 0.0;
    this.note = map['Notes'];
    this.MngUsername = map['MngUsername'];
    this.showRestau = map['ShowRst'] == 1;
    this.showRate = map['ShowRate'] == 1;
    this.typeName = map['RestaurantTypeName'];

    if (this.typeName != null) {
      if (this.typeName.contains('مطاعم')) {
        this.name = 'مطعم ${map['RestaurantName']}';
      } else {
        this.name = 'مقهي ${map['RestaurantName']}';
      }
    }else{
      if (this.typeId == 1) {
        this.name = 'مطعم ${map['RestaurantName']}';
      } else {
        this.name = 'مقهي ${map['RestaurantName']}';
      }
    }

    lRate  = map['LRate'];
    this.startDate = DateTime.fromMicrosecondsSinceEpoch(int.parse(map['Sdate'].substring(6,map['Sdate'].length-2))  * 1000);
    if(map['Edate'] != null)
    this.endDate = DateTime.fromMicrosecondsSinceEpoch(int.parse(map['Edate'].substring(6,map['Edate'].length-2))  * 1000);

    mobile = map['Mobile'];
    tel = map['Tel'];

  }

}


class RestoDao{

  Future<dynamic> getRestoImages(int restoId,Function completion){
    String url = '$apiUrl/RestaurantPic';
    var params = {"RestaurantID" : restoId};

    return http.postToServer(url: url, body: params, executeBody: (map){
      List<RestoImage> imsg = List();
      for(var item in map){
        imsg.add(RestoImage.fromJson(item));
      }
      completion(imsg);
    });
  }

  Future<dynamic> getAllRestaus(String restauid ,Function executeCode) async{
    String url = '$apiUrl/Restaurantkitchen';

    Map<String,dynamic> params = {"kitchenTypeID" : restauid};

    http.postToServer(url: url, body: params, executeBody: (map){
      print(map);
      List<Restaurant> restaus = List();
      for(var restuMap in map){
        var restu = Restaurant.fromJson(restuMap);
//        if(restu.endDate == null || restu.endDate.compareTo(DateTime.now()) >= 0 )
           restaus.add(restu);
      }
      executeCode(restaus);
    });

  }

  Future<dynamic> uploadUserInvoice(File image,int userId, int restaurantId,String total, Function completion){
    String url = '$apiUrl/UploadInvoiceRate';
    Map<String, String> params = {
      "ClientID" : "$userId",
      "RestaurantID" : "$restaurantId",
      "Total" : "$total"
    };

    http.uploadInvoice(url: url, body: params, imageFile: image, executeCode: (map){
      completion(map);
    });
  }
}