

import 'package:restu_ratingflutter_app/utils/app_urls.dart';
import 'package:restu_ratingflutter_app/utils/connect_to_server.dart';

class KitchenType{
  int typeId;
  String typeName;

  KitchenType(this.typeId,this.typeName);

  KitchenType.fromJson(Map<String,dynamic> map){
    this.typeId = map['kitchenTypeID'];
    this.typeName = map['kitchenTypeName'];
  }
}

class KitchenTypeDao{

  Future<dynamic> getKitchenTypes(Function completion){

    String url = '$apiUrl/kitchenTypeList';

    return postToServer(url: url, body: null, executeBody: (map){

      List<KitchenType> list = List();

      for(Map<String,dynamic> item in map){
        list.add(KitchenType.fromJson(item));
//        print('++++++++++++++++++++++++++++++++++++++++++++ ${KitchenType.fromJson(item).typeName}');
      }
      completion(list);
    });
  }

}