import 'package:path/path.dart';
import 'package:async/async.dart';
import 'dart:io';
import 'package:http/http.dart' as https;
import 'dart:convert';

import 'package:restu_ratingflutter_app/utils/connect_to_server.dart' as http;
import 'package:restu_ratingflutter_app/utils/app_urls.dart';

class Question{
  int q_id;
 Map<String,String> questions = Map();

  Question();
  Question.value(this.q_id,this.questions);

  Question.fromJson(Map<String,dynamic> map){

    this.q_id = map['QuestionID'];

    print('=+++++=============+++qqqq $q_id $map');

    this.questions['Question1'] = map['Question1'];
    this.questions.putIfAbsent('Question2', ()=> map['Question2']);
    this.questions.putIfAbsent('Question3', ()=> map['Question3']);
    this.questions.putIfAbsent('Question4', ()=> map['Question4']);
    this.questions.putIfAbsent('Question5', ()=> map['Question5']);
  }
}

class QuestionsDao{

  Future<dynamic> getQuestions(Function executeCode) async{
    String url = '$apiUrl/Questions';
    
    http.postToServer(url: url, body: null, executeBody: (map){
      Question questions;
      for(var q in map){
        questions = Question.fromJson(q);
      }
      executeCode(questions);
    });
  }

  Future<dynamic> rateRestaurant(List questions,String clientId,String restoId,String comment,Function executeCode) async {

    String url = '$apiUrl/ClientRate';
    Map<String,dynamic> params = {
      "ClientID" : clientId ,
      "RestaurantID" : restoId,
      "Question1" : "${questions[0]}" ,
      "Question2" : "${questions[1]}",
      "Question3" : "${questions[2]}",
      "Question4" : "${questions[3]}",
      "Question5" : "${questions[4]}",
      "Comment":comment
    };

    print(params);
    http.postToServer(url: url, body: params, executeBody: (map){
      print(map);
      executeCode(map['Msg']);
    });
  }

  Future<dynamic> localRate(File image,List questions,String restoId,String comment,String name,String phone,Function executeCode) async {

    String url = '$apiUrl/LocalRate';
    Map<String,String> params = {
      "ClientName" : name,
      "Mobile" : phone,
      "RestaurantID" : restoId,
      "Question1" : "${questions[0]}" ,
      "Question2" : "${questions[1]}",
      "Question3" : "${questions[2]}",
      "Question4" : "${questions[3]}",
      "Question5" : "${questions[4]}",
      "Comment":comment,

    };

//    return http.uploadInvoice(url: url, body: params, imageFile: image, executeCode: (map){
//      executeCode(map['Msg']);
//    });

    return http.postToServer(url: url, body: params, executeBody: (map){
       executeCode(map['Msg']);
    });
  }


  Future<dynamic> uploadInvoice(File imageFile,Function executeCode) async{
    String url = '$apiUrl/UploadInvoice';

    // open a bytestream
    var stream = new https.ByteStream(DelegatingStream.typed(imageFile.openRead()));
    // get file length
    var length = await imageFile.length();

    // string to uri
    var uri = Uri.parse(url);

    // create multipart request
    var request = new https.MultipartRequest("POST", uri);

    // multipart that takes file
    var multipartFile = new https.MultipartFile('file', stream, length,
        filename: basename(imageFile.path));

    // add file to multipart
    request.files.add(multipartFile);


    // send
    var response = await request.send();
    print(response.statusCode);

    // listen for response
    response.stream.transform(utf8.decoder).listen((value) {
      print(value);
      executeCode(value);
    });
  }
}