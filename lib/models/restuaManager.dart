
class RestauManager{
  int RestaurantID;
  String RestaurantName;
  int RestaurantTypeID;
  String Address;
  String MainPicture;
  String MngUsername;
//  "MgrPassword": "1fNjFoBXq3vt6l/z9K2oxsMgPVJlZWDIYGXchf2SG+Q=",
  int ShowRst;
  int ShowRate;
  String Lat;
  String long;
  int Rate;
  String Notes;
  DateTime Sdate;
  DateTime Edate;
  int LRate;
  String Mobile;
  String Tel;

  RestauManager.fromJson(Map<String,dynamic> map){
    this.RestaurantID = map['RestaurantID'];
    this.RestaurantName = map['RestaurantName'];
    this.RestaurantTypeID = map['RestaurantTypeID'];
    this.Address = map['Address'];
    this.MainPicture = map['MainPicture'];
    this.MngUsername = map['MngUsername'];
    this.ShowRst = map['ShowRst'];
    this.ShowRate = map['ShowRate'];
    this.Lat = map['Lat'];
    this.long = map['long'];
    this.Rate = map['Rate'];
    this.Notes = map['Notes'];
    this.Sdate = DateTime.fromMicrosecondsSinceEpoch(int.parse(map['Sdate'].substring(6,map['Sdate'].length-2))  * 1000);
    if(map['Edate'] != null)
      this.Edate = DateTime.fromMicrosecondsSinceEpoch(int.parse(map['Edate'].substring(6,map['Edate'].length-2))  * 1000);

    this.LRate = map['LRate'];
    this.Mobile = map['Mobile'];
    this.Tel = map['Tel'];
//    this.RestaurantName = map[''];

  }
}