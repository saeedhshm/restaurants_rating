import 'package:restu_ratingflutter_app/utils/connect_to_server.dart' as http;
import 'package:restu_ratingflutter_app/utils/app_urls.dart';

class OfferPrice{
  int OfferID;
  int RecipeID;
  int RecipeGroupID;
  int RestaurantID;
  String RecipeName;
  String RecipePic;
  dynamic offerPrice;
  double Price;
  double  Priceratio;

  OfferPrice.fromJSON(Map<String,dynamic> map){
    print("----------------> ${map['Price']} ${map['Price'].runtimeType}");
    OfferID = map['OfferID'];
    RecipeID = map['RecipeID'];
    RecipeGroupID = map['RecipeGroupID'];
    RestaurantID = map['RestaurantID'];
    RecipeName = map['RecipeName'];
    RecipePic = map['RecipePic'];
    offerPrice = map['OfferPrice'];
    Price = map['Price'].toDouble() ;
    Priceratio = map['Priceratio'];

    print('----------------> $Price');

  }
}

class OfferPriceDao{

  Future<dynamic> getAllOfferPrice(int offerId,Function completion){

    String url = '$apiUrl/OffersPrice';

    Map<String,dynamic> params = {"OfferID" : "$offerId"};

    return http.postToServer(url: url, body: params, executeBody: (map){
      print(map);
      List<OfferPrice> offerPrices = List();
      
      for(var item in map){
        offerPrices.add(OfferPrice.fromJSON(item)) ;
      }
       completion(offerPrices);
    });
  }
}