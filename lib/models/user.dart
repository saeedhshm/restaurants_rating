import 'package:restu_ratingflutter_app/utils/statics.dart';
import 'package:shared_preferences/shared_preferences.dart';

class User{
  String email;
  String password;
  int userId;
  String name;
  String mobile;
  String birthdate;
  DateTime registerDate;
  bool isStopped;
  int points;

  User.login({this.email,this.password});


  User.name({this.email, this.password, this.name, this.mobile});

  User.fromJson(Map<String,dynamic> map) {
//    String userJson = json.encode(map);
    print('====================================');
    print(map);
    print('=====================================');
   this.userId = map['ClientID'];
   this.name = map['ClientName'];
   this.mobile = map['Mobile'];
   this.isStopped = map['Stoped'] == 1;
   this.points = map['ClientPoint'];
   this.email = map['Email'];
//   this.password = map['Password'];
   this.birthdate = map['BirthDate'];
   this.registerDate = DateTime.fromMicrosecondsSinceEpoch(int.parse(map['RegisterDate'].substring(6,map['RegisterDate'].length-2))  * 1000);

   if(!isStopped) {
     _saveUserId(this.userId,this.password,this.email);
   }

  }

  _saveUserId(int id,String password,String email) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt('user_id', id);

  }
}