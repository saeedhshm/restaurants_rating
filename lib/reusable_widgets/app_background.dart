import 'package:flutter/material.dart';


class Background extends StatelessWidget {

  final String backgroundImage;

  Background(this.backgroundImage);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width:  MediaQuery.of(context).size.width,
      child:  DecoratedBox(

        decoration: BoxDecoration(

//                        color: Colors.yellow,
          image: DecorationImage(
            image: AssetImage(backgroundImage),
            fit: BoxFit.fill,

            // ...
          ),
          // ...
        ),
      ),

    );
  }
}
