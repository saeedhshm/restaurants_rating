import 'package:flutter/material.dart';

import 'package:restu_ratingflutter_app/utils/colors.dart';

enum KeyboardType {
  KEYBOARD_PHONE,
  KEYBOARD_NUMBER,
  KEYBOARD_EMAIL,
  KEYBOARD_URL,
  KEYBOARD_TEXT
}

class EditText extends StatelessWidget {

  final TextEditingController txtEditingCrtl;
  final Function validator;
  final Function onSave;
  final String hintText;
  final String labelText;
  final KeyboardType keyboardType;
  final isEnabled;
  final int maxLine;

  bool secureText;

  var _keyboardType = [
    TextInputType.phone,
    TextInputType.number,
    TextInputType.emailAddress,
    TextInputType.url,
    TextInputType.text,
  ];



  EditText(
  {
    this.txtEditingCrtl,
    this.validator,
    this.hintText,
    this.labelText,
    this.secureText = false,
    this.onSave,
    this.keyboardType = KeyboardType.KEYBOARD_TEXT,
    this.isEnabled = true,
    this.maxLine = 1
  }
      );

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextFormField(
        keyboardType: _keyboardType[keyboardType.index],
        controller: txtEditingCrtl,
        validator: validator,
        obscureText: secureText,
        enabled: isEnabled,
        maxLines: maxLine,
        onSaved:onSave ,
//        style: TextStyle(fontFamily: ""),
        decoration: InputDecoration(
//            enabledBorder: OutlineInputBorder(
//              // width: 0.0 produces a thin "hairline" border
//              borderSide: BorderSide(color: appColor, width: 0.0),
////              borderRadius: BorderRadius.all(Radius.circular(50.0))
//            ),
//            border: const OutlineInputBorder(),

            hintText: hintText,
            labelText: labelText
        ),

      ),
    );
  }
}
