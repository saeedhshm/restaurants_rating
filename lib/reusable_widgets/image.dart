import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:async';

class MyImage extends StatelessWidget {

  final double width;
  final double height;
  final String imageTitle;

  MyImage({this.width = 20.0, this.height = 20.0,this.imageTitle});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width:  width,
      child:  DecoratedBox(

        decoration: BoxDecoration(

//                        color: Colors.yellow,
          image: DecorationImage(
            image: AssetImage(imageTitle),
            fit: BoxFit.fill,

            // ...
          ),
          // ...
        ),
      ),

    );
  }
}


void chooseImage(BuildContext context,Function setImage){
  showDialog(
      context: context,
      builder: (_) => new AlertDialog(
//                                title: new Text(""),
//                                content: new Text("This is my content"),
        actions: <Widget>[
          Container(child: FlatButton(onPressed: ()async{
            var image = await ImagePicker.pickImage(source: ImageSource.camera,maxWidth: 1920.0,maxHeight: 1080.0);
            Navigator.of(context).pop();
            setImage(image);
          }, child: Text("الكاميرا"))),
//                                  (),
          Container(child: FlatButton(onPressed: ()async{
            var image = await ImagePicker.pickImage(source: ImageSource.gallery,maxWidth: 1920.0,maxHeight: 1080.0);
            Navigator.of(context).pop();
            setImage(image);
          }, child: Text("الصور"))),
        ],
      )
  );
}

Widget activityIndicator(){
  return CircularProgressIndicator(valueColor:new AlwaysStoppedAnimation<Color>(Colors.white),);
}