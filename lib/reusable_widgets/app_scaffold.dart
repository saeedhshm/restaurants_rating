import 'package:flutter/material.dart';

import 'package:restu_ratingflutter_app/reusable_widgets/edit_text.dart';
import 'package:restu_ratingflutter_app/reusable_widgets/app_background.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:restu_ratingflutter_app/screens/all_resto.dart';
import 'package:restu_ratingflutter_app/screens/replace_points.dart';
import 'package:restu_ratingflutter_app/screens/login.dart';
import 'package:restu_ratingflutter_app/screens/user_profile.dart';
import 'package:restu_ratingflutter_app/screens/messeges.dart';
import 'package:restu_ratingflutter_app/screens/current_offers.dart';
import 'package:restu_ratingflutter_app/screens/notifications.dart';
import 'package:restu_ratingflutter_app/screens/upload_user_invoice.dart';
import 'package:restu_ratingflutter_app/screens/buying_points_request.dart';

class AppScaffold extends StatelessWidget {

  final String screenTitle;
  final Container container;
  final String bg;
  final BuildContext context;
  final bool isAllResto;
  final Function filterAction;

  final List<Widget> actions;

  final bool isManager;
  List<CustomPopupMenu> choices= <CustomPopupMenu>[

    CustomPopupMenu(title: 'تسجيل الدخول', status: 3),


  ];


  CustomPopupMenu _selectedChoices ;
  void _select(CustomPopupMenu choice) async{

    switch (choice.status) {

      case 0 : {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ReplacePoints()),
        );
      }
      break;

      case 1 : {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => UserProfile()),
        );
      }
      break;
      case 2 : {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setInt('user_id', -1);
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
            AllRestos()), (Route<dynamic> route) => false);
      }
      break;
      case 3 : {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Login()),
        );
      }
      break;
      case 4 : {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => PointMessages()),
        );
      }
      break;
      case 5 : {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => CurrentOffers()),
        );
      }
      break;
      case 6 : {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => Notifications()),
        );
      }
      break;
      case 7 : {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => UploadInvoice()),
        );
      }
      break;
      case 8 : {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => BuyingPointsRequest()),
        );
      }
      break;

      default :{}
      break;
    }

  }

  _setChoices() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int user_id = (prefs.getInt('user_id') ?? -1);

    if(user_id > -1){
      choices = <CustomPopupMenu>[
        CustomPopupMenu(title: 'تبديل النقاط', status: 0),
        CustomPopupMenu(title: 'الصفحة الشخصية', status: 1),
        CustomPopupMenu(title: 'النقاط المبدلة', status: 4),
        CustomPopupMenu(title: 'العروض', status: 5),
        CustomPopupMenu(title: 'الإشعارات', status: 6),
        CustomPopupMenu(title: 'رفع فاتورة الشراء', status: 7),
        CustomPopupMenu(title: 'تبديل نقاط الشراء', status: 8),
        CustomPopupMenu(title: 'تسجيل الخروج', status: 2),


      ];
    }else if(isManager) {
      choices = <CustomPopupMenu>[
      CustomPopupMenu(title: 'تسجيل الخروج', status: 2),
      ];
    }else{
      choices = <CustomPopupMenu>[

        CustomPopupMenu(title: 'تسجيل الدخول', status: 3),


      ];
    }

  }
  AppScaffold(this.context,{this.filterAction,this.isAllResto = false,this.screenTitle,this.container,this.bg = 'assets/images/screens_bg.png',this.isManager = false,this.actions}){
    _setChoices();
  }

  @override
  Widget build(BuildContext context) {

    _selectedChoices = choices[0];

    if(actions != null){
      actions.add(PopupMenuButton<CustomPopupMenu>(
        elevation: 1,
//            initialValue: choices[0],
        onCanceled: () {
          print('You have not chossed anything');
        },
        tooltip: 'This is tooltip',
        onSelected: _select,
        itemBuilder: (BuildContext context) {
          return choices.map((CustomPopupMenu choice) {
            return PopupMenuItem<CustomPopupMenu>(
              value: choice,
              child: Text(choice.title),
            );
          }).toList();
        },
      )
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(screenTitle,style: TextStyle(fontSize: 17.0),),
//        backgroundColor: appColor,
        centerTitle: !isAllResto,

        actions:  actions

      ),
      body: Stack(children: <Widget>[
        Background(bg),
        container
      ],),

    );
  }
}
class CustomPopupMenu {
  CustomPopupMenu({this.title, this.status});

  String title;
  int status;
}