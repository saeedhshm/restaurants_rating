import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:restu_ratingflutter_app/models/notif_message.dart';

import 'package:restu_ratingflutter_app/utils/statics.dart';
import 'package:restu_ratingflutter_app/utils/colors.dart';

import 'package:restu_ratingflutter_app/reusable_widgets/app_scaffold.dart';

class Notifications extends StatefulWidget {
  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {

  int user_id = -1;
  List<NotifMessage> noifications;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _awaitFor().then((v) {
//      print("=-===============->> ${v.}");
      setState(() {
        if (user_id != -1) {
          NotifMessageDao notifMessageDao = NotifMessageDao();
          notifMessageDao.getNofs(-1, (List notifs) {
            noifications = notifs;
            setState(() {

            });
          }).then((v){
            print('=-===============->> then ${v}');
          });
        }
      });
    });
  }

    Future<void> _awaitFor() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    user_id = (prefs.getInt('user_id') ?? -1);
    print("=-===============->> await $user_id");

  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      context,
      screenTitle: 'الإشعارات',
      bg: 'assets/images/bg_dark.png',
      container: Container(
        child: noifications == null ?
        Center(child: CircularProgressIndicator(),) :
        noifications.length == 0 ? Center(child: Text('لا توجود اشعارات جديدة',style: TextStyle(
            fontSize: 18.0,color: appColor,fontWeight: FontWeight.bold
        )),):
        ListView.builder(
          itemCount: noifications.length,
            itemBuilder: (context, index){
            var notifMsg = noifications[index];
            return Card(
              child: Container(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    Text('الجائزة : ${notifMsg.Prize}'),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,

                          children: <Widget>[
                            Text('اسم العميل'),
                            Text('المطعم'),
                            Text('النقاط المستبدلة'),
                            Text('الرسالة'),
                            Text('تاريخ الارسال')
                          ],
                        ),
                        SizedBox(width: 10.0,),
                        Flexible(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(notifMsg.ClientName),
                              Text(notifMsg.RestaurantName),
                              Text('${notifMsg.Points}'),
                              Text(notifMsg.Mssge),
                              Text(dateTime(notifMsg.Sendtime))
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            );
            }),
      ),
    );
  }
}
