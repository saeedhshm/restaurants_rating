import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';
import 'package:email_validator/email_validator.dart';

import 'package:restu_ratingflutter_app/utils/colors.dart';

import 'package:restu_ratingflutter_app/models/user_dao.dart';
import 'package:restu_ratingflutter_app/models/user.dart';

import 'package:restu_ratingflutter_app/reusable_widgets/app_scaffold.dart';
import 'package:restu_ratingflutter_app/reusable_widgets/edit_text.dart';
import 'all_resto.dart';

class Registeration extends StatefulWidget {

  final User user;
  Registeration(this.user);

  @override
  _RegisterationState createState() => _RegisterationState(this.user);
}

class _RegisterationState extends State<Registeration> {


   TextEditingController emailCtr = TextEditingController();
   TextEditingController passwordCtrl = TextEditingController();
   TextEditingController userNameCtrl = TextEditingController();
   TextEditingController phoneCtrl = TextEditingController();
   TextEditingController repeatPasswordCtrl = TextEditingController();

  final formKey = GlobalKey<FormState>();
  var sendingData = false;

//  final

  final User user;
  _RegisterationState(this.user){

    if(user != null) {
      _getUserPassword();
      emailCtr = TextEditingController(text: user.email);
      userNameCtrl = TextEditingController(text: user.name);
      phoneCtrl = TextEditingController(text: user.mobile);
    }
  }

  _getUserPassword() async{

    if(user != null) {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      user.password = prefs.getString('password') ?? "";
      passwordCtrl = TextEditingController(text: user.password );
      repeatPasswordCtrl = TextEditingController(text: user.password );
      setState(() {

      });
    }
  }


  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      context,
      screenTitle: user == null ? "تسجيل" : 'تعديل الحساب',
      container: Container(
//        color: Colors.yellow,
        width: MediaQuery.of(context).size.width,
        child: Padding(
          padding: const EdgeInsets.only( left: 50.0,right: 50.0),
          child: SingleChildScrollView(
//          crossAxisAlignment: CrossAxisAlignment.center,
           child: Column(
             children: <Widget>[
               SizedBox(height: 20.0,),

               Container(
//          width: 20.0,
                 child: Text(user == null ? "مستخدم جديد" : '', style: TextStyle(fontWeight: FontWeight.bold,fontSize: 22.0,color: appColor),),
               ),
               SizedBox(height: 0.0,),
               Form(
                 key: formKey,
                   child: Column(
                     children: <Widget>[
                       EditText(
                         hintText: "اسم المستخدم",
                         labelText: "اسم المستخدم",
                         txtEditingCrtl: userNameCtrl,
                         validator: (String value){
                           if(value.isEmpty){
                             return "لاتترك هذا الحقل فارغاً";
                           }
                         },
                       ),
                       SizedBox(height:1.0),
                       EditText(
                         hintText: "رقم الهاتف",
                         labelText: "رقم الهاتف",

                         txtEditingCrtl: phoneCtrl,
                         validator: (String value){
                           if(value.isEmpty){
                             return "لاتترك هذا الحقل فارغاً";
                           }

                         },
                       ),
                       SizedBox(height: 1.0,),
                       EditText(
                         hintText: "youremail@mail.com",
                         labelText: "البريد الالكتروني",
                         txtEditingCrtl: emailCtr,
                         validator: (String value){
                           if(value.isEmpty){
                             return "لاتترك هذا الحقل فارغاً";
                           }
//                           print("=========================+++++++++++ ${EmailValidator.Validate(value, true)}");
                           if(!EmailValidator.validate(value.trim())){
                             return "من فضلك ادخل بريد الكتروني صحيح";
                           }
                         },
                       ),
                       SizedBox(height:1.0),
                       EditText(
                         hintText: "********",
                         labelText: "كلمة المرور",
                         secureText: true,
                         txtEditingCrtl: passwordCtrl,
                         validator: (String value){

                           if(value.isEmpty){
                             return "لاتترك هذا الحقل فارغاً";
                           }

                         },
                       ),
                       SizedBox(height:1.0),
                       EditText(
                         hintText: "********",
                         labelText: "كلمة المرور",
                         secureText: true,
                         txtEditingCrtl: repeatPasswordCtrl,
                         validator: (String value){

                           if(value.isEmpty){
                             return "لاتترك هذا الحقل فارغاً";
                           }

                         },
                       ),
                       SizedBox(height: 15.0,),

                       InkWell(
                         onTap: (){



                           if(formKey.currentState.validate()){
                             if(passwordCtrl.text != repeatPasswordCtrl.text){
                               Toast.show('كلمتا المرور غير متطابقتين',context , duration: 3, gravity:  Toast.BOTTOM);
                             }else{
                               sendingData = true;
                               setState(() {
                               });

                               if(user == null){

                               User user = User.name(email: emailCtr.text,name: userNameCtrl.text,password: passwordCtrl.text,mobile: phoneCtrl.text);
                               UserDao dao = UserDao(user);
                               dao.registerUser((int msgId,String msg){
                                 sendingData = false;
                                 setState(() {
                                 });

                                 Toast.show(msg,context , duration: 3, gravity:  Toast.BOTTOM,completion: (){
                                   if(msgId == 1){
                                     Navigator.pushAndRemoveUntil(
                                       context,
                                       MaterialPageRoute(builder: (context) => AllRestos()),
                                         ModalRoute.withName("/Home")
                                     );
                                   }
                                 });
                               });
                               }else{
                                 user.password = passwordCtrl.text;
                                  UserDao dao = UserDao(user);
                                  dao.updateUserProfile((msg){
                                    Toast.show(msg,context , duration: 3, gravity:  Toast.BOTTOM,completion: (){
                                      Navigator.pop(context);
                                    });
                                  });
                               }
                             }
                           }


                         },
                         child: Container(
                           width: double.infinity,
                           decoration: BoxDecoration(
                               color: appColor,
                             borderRadius: BorderRadius.all(Radius.circular(8.0))
                           ),
                           height: 40.0,

                           child: Center(
                             child: sendingData ?
                             CircularProgressIndicator(valueColor:new AlwaysStoppedAnimation<Color>(Colors.white),) :
                             Center(child: Text( user == null ? "تسجيل" : "تعديل",style: TextStyle(color: Colors.white),)),
                           ),
                         ),

                       ),
                       SizedBox(height: 30.0,)

                     ],
                   ),
               )
             ],
           ),

          ),
        ),
      ),
    );
  }
}
