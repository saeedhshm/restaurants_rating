import 'package:flutter/material.dart';

import 'package:restu_ratingflutter_app/reusable_widgets/app_scaffold.dart';
import 'package:restu_ratingflutter_app/utils/colors.dart';
import 'package:restu_ratingflutter_app/models/user_dao.dart';
import 'package:toast/toast.dart';

class ReplacePoints extends StatefulWidget {
  @override
  _ReplacePointsState createState() => _ReplacePointsState();
}

class _ReplacePointsState extends State<ReplacePoints> {

  bool sendingData = false;
  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      context,
      screenTitle: "تبديل النقاط",
      bg: "assets/images/bg_rest.png",
      container: Container(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            Text('يمكنك تبديل نقاطك المتاحة الي طلبات من المطاعم المختلفة',textAlign: TextAlign.center,),
            SizedBox(height: 25.0,),
            Text('اضغط ارسال الطلب ليتم ارسال طلبك'),

            SizedBox(height: 50.0,),
            InkWell(
              onTap: (){

                sendingData = true;
                setState(() {

                });
                UserDao dao = UserDao(null);
                dao.replacePoints((msg){
                  Toast.show(msg,context , duration: 3, gravity:  Toast.BOTTOM);
                  sendingData = false;
                  setState(() {
                  });
                });
              },
              child: Container(
                width: double.infinity,
                decoration: BoxDecoration(
                    color: appColor,
                    borderRadius: BorderRadius.all(Radius.circular(8.0))
                ),
                height: 40.0,

                child: Center(
                  child: sendingData ?
                  CircularProgressIndicator(valueColor:new AlwaysStoppedAnimation<Color>(Colors.white),) :
                  Center(child: Text("ارسال الطلب",style: TextStyle(color: Colors.white),)),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
