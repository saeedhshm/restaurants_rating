import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:restu_ratingflutter_app/models/order.dart';

import 'package:restu_ratingflutter_app/utils/app_urls.dart';
import 'package:restu_ratingflutter_app/utils/colors.dart';
import 'package:restu_ratingflutter_app/utils/styles.dart' as styles;
import 'package:restu_ratingflutter_app/models/recipe.dart';
import 'package:restu_ratingflutter_app/models/resturant.dart';

import 'package:restu_ratingflutter_app/reusable_widgets/app_background.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

import 'restaurant_info.dart';
import 'my_current_order.dart';


class RecipeItems extends StatefulWidget {
  final RecipeGroup recipeGroup;
  final Restaurant restaurant;

  RecipeItems(this.recipeGroup, this.restaurant);
  @override
  _RecipeItemsState createState() => _RecipeItemsState(recipeGroup, restaurant);
}

class _RecipeItemsState extends State<RecipeItems> {

  int user_id = -1;
  final RecipeGroup recipeGroup;
  final Restaurant restaurant;

  List<Recipe> recipes;

  _RecipeItemsState(this.recipeGroup, this.restaurant){
    _awaitFor();
  }



  Future<void> _awaitFor() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    user_id = (prefs.getInt('user_id') ?? -1);
    print("=-===============->> await $user_id");

  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    RecipeDao dao = RecipeDao();
    dao.getRecipe(recipeGroup.RestaurantID, recipeGroup.RecipeGroupID,
        (recipes) {
      this.recipes = recipes;
      setState(() {

      });
    });
  }

  @override
  Widget build(BuildContext context) {
    int orderItemsCount = 0;
    for(OrderItem item in orderItems){
      orderItemsCount += item.count;
    }
    return Scaffold(
      appBar: AppBar(
        title: Row(
          children: <Widget>[
            CircleAvatar(
              child: ClipOval(
                child: Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: FadeInImage.assetNetwork(
                      placeholder: 'assets/images/logo.png',
                      image: "$imgUrl/${restaurant.mainImg}",
                      fit: BoxFit.fill,
                    )),
              ),
//                      backgroundColor: Colors.transparent,
              radius: 22.0,
            ), //image end
            SizedBox(
              width: 8.0,
            ),
            Text(restaurant.name,style: styles.appBarTextStyle,),
          ],
        ),
        actions: <Widget>[
          InkWell(
            onTap: (){
              if(orderItemsCount > 0){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyCurrentOrder()),
                );
              }
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Stack(
                children: <Widget>[
                  Icon(Icons.shopping_cart,color: Colors.white,),
                  orderItemsCount > 0 ?CircleAvatar(
                    backgroundColor: Colors.black,
                    child: Text('$orderItemsCount',style: TextStyle(color: Colors.white,fontSize: 10.0),),
                    radius: 8.0,
                  ) : Container()
                ],
              ),
            ),
          )
        ],
      ),
      body: Stack(
        children: <Widget>[
          Background('assets/images/bg_dark.png'),
          recipes == null ? Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          ) : Container(
            child: Column(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.width / 2.2,
                  color: Colors.white,
                  child: Stack(
                    alignment: Alignment.bottomRight,
                    children: <Widget>[
                      FadeInImage.assetNetwork(
                        placeholder: 'assets/images/logo.png',
                        image: "$recipeGroupImg/${recipeGroup.RecipeGroupPic}",
                        fit: BoxFit.fill,
                        height: double.infinity,
                        width: double.infinity,
                      ),
                      Container(
                        color: Colors.black.withOpacity(0.5),
                      width: double.infinity,
                        child: Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Text(
                            recipeGroup.RecipeGroupName,
                            style: TextStyle(
                                color: Colors.white, fontWeight: FontWeight.bold),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 0.0,
                ),
                Flexible(
                    child: Container(
                  width: double.infinity,
                  child: ListView.builder(
                    itemCount: recipes.length,
                      itemBuilder: (context,index){
                      var recipe = recipes[index];
                      return Card(
                        color: Colors.white.withOpacity(1.0),
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Row(
//                              crossAxisAlignment: CrossAxisAlignment.center,
//                              mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              CircleAvatar(
                                child: ClipOval(
                                  child: Container(
                                      height: MediaQuery.of(context).size.height,
                                      width: MediaQuery.of(context).size.width,
                                      child: FadeInImage.assetNetwork(
                                        placeholder: 'assets/images/logo.png',
                                        image: "$recipeImage/${recipe.RecipePic}",
                                        fit: BoxFit.fill,
                                      )),
                                ),
//                      backgroundColor: Colors.transparent,
                                radius: 30.0,
                              ),
                              SizedBox(width: 10.0,),
                              Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(recipe.RecipeName,style: TextStyle(color: appColor,fontSize: 17.0,fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                                  SizedBox(height: 10.0,),
                                  Text('السعر : ${recipe.Price} ريال',style: TextStyle(color: appColor,fontSize: 12.0),),
                                ],
                              ),
                              Flexible(child: Container(color: Colors.amber,),),
                              InkWell(
                                onTap: (){
                                  if(user_id > -1){

                                    if(orderRestaurant != null && (orderRestaurant.id != restaurant.id))
                                      orderItems.clear();

                                    for(int i=0; i< orderItems.length; i++) {

                                      if (orderItems[i].recipeId ==
                                          recipe.RecipeID) {
                                        orderItems[i].count += 1;
                                        setState(() {
                                          print('rtrtkjgdfg d dfg dfsg jdfshg kjdfg.dfgjdf.gjfdhhfkdjhgkhdsgkjh');
                                        });
                                        return;
                                      }
//                                        print('=-=-=-=-=-=-=-=-=>>>>${orderItems[i].recipeName} ${orderItems[i].count}');
                                    }
                                    orderRestaurant = this.restaurant;
                                    var orderItem = OrderItem();
                                    orderItem.price = '${recipe.Price}';
                                    orderItem.recipeGroup = recipeGroup.RecipeGroupName;
                                    orderItem.count = 1;
                                    orderItem.recipeName = recipe.RecipeName;
                                    orderItem.recipeId = recipe.RecipeID;
                                    orderItem.restaurantId = recipeGroup.RestaurantID;
                                    orderItem.orderImage = recipe.RecipePic;
                                    orderItems.add(orderItem);
                                    setState(() {

                                    });
                                    print(orderItems.length);
                                  }else{
                                    Toast.show('يجب تسجيل الدخول حتي تتمكن من طلب وجبة',context , duration: 3, gravity:  Toast.BOTTOM);
                                  }
                                },
                                child: Container(
                                  padding: EdgeInsets.all(8.0),
//                                    color: appColor,
                                    child: Icon(Icons.add_shopping_cart,color: appColor,)

                                ),
                              )
                            ],
                          ),
                        ),
                      );
                      },
                  ),
                )),
              ],
            ),
          )
        ],
      ),
    );
  }
}
