import 'package:flutter/material.dart';
import 'package:restu_ratingflutter_app/utils/statics.dart';

import 'package:restu_ratingflutter_app/utils/styles.dart' as styles;
import 'package:restu_ratingflutter_app/utils/colors.dart';
import 'package:restu_ratingflutter_app/utils/app_urls.dart';

import 'package:restu_ratingflutter_app/models/order.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

import 'restaurant_info.dart';

class MyCurrentOrder extends StatefulWidget {
  @override
  _MyCurrentOrderState createState() => _MyCurrentOrderState();
}

class _MyCurrentOrderState extends State<MyCurrentOrder> {


  _MyCurrentOrderState(){
    _awaitFor();
  }

  Future<void> _awaitFor() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    user_id = (prefs.getInt('user_id') ?? -1);
    print("=-===============->> await $user_id");

  }

  @override
  Widget build(BuildContext context) {

    int orderItemsCount = 0;
    double totalPrice = 0.0;
    for(OrderItem item in orderItems){
      orderItemsCount += item.count;
      totalPrice += (item.count * double.parse(item.price));
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("طلبي",style: styles.appBarTextStyle,),

        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Stack(
              children: <Widget>[
                Icon(Icons.shopping_cart,color: Colors.white,),
                orderItemsCount > 0 ?CircleAvatar(
                  backgroundColor: Colors.black,
                  child: Text('$orderItemsCount',style: TextStyle(color: Colors.white,fontSize: 10.0),),
                  radius: 8.0,
                ) : Container()
              ],
            ),
          )
        ],
      ),
      body: Container(
        height: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg_dark.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: orderItemsCount > 0 ? Container(

              child: Column(
                children: <Widget>[
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: <Widget>[
                          CircleAvatar(
                            child: ClipOval(
                              child: Container(
                                  height: MediaQuery.of(context).size.height,
                                  width: MediaQuery.of(context).size.width,
                                  child: FadeInImage.assetNetwork(
                                    placeholder: 'assets/images/logo.png',
                                    image: "$imgUrl/${orderRestaurant.mainImg}",
                                    fit: BoxFit.fill,
                                  )),
                            ),
//                      backgroundColor: Colors.transparent,
                            radius: 22.0,
                          ), //image end
                          SizedBox(
                            width: 8.0,
                          ),
                          Text(orderRestaurant.name,style: styles.appBarTextStyle,),
                        ],
                      ),
                    ),
                  ),
//                      SizedBox(height: 5.0,),
                  Column(
                    children: orderItems.map((order){
                      return Card(
                        color: Colors.white.withOpacity(1.0),
                        child: Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Row(
//                              crossAxisAlignment: CrossAxisAlignment.center,
//                              mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              CircleAvatar(
                                child: ClipOval(
                                  child: Container(
                                      height: MediaQuery.of(context).size.height,
                                      width: MediaQuery.of(context).size.width,
                                      child: FadeInImage.assetNetwork(
                                        placeholder: 'assets/images/logo.png',
                                        image: "$recipeImage/${order.orderImage}",
                                        fit: BoxFit.fill,
                                      )),
                                ),
//                      backgroundColor: Colors.transparent,
                                radius: 30.0,
                              ),
                              SizedBox(width: 10.0,),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(order.recipeName,style: TextStyle(color: appColor,fontSize: 17.0,fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                                  SizedBox(height: 10.0,),
                                  Text('السعر : ${order.price} ريال',style: TextStyle(color: appColor,fontSize: 12.0),),
                                ],
                              ),
                              Flexible(child: Container(color: Colors.amber,),),
                              Column(

                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      InkWell(
                                        onTap: (){
                                          if(order.count > 1) {
                                            order.count -= 1;
                                            setState(() {

                                            });
                                          }
                                        },
                                        child: Container(
                                            padding: EdgeInsets.all(5.0),
//                                    color: appColor,
                                            child: Icon(Icons.expand_more,color: appColor,size: 20.0,)

                                        ),
                                      ),
                                      SizedBox(width: 2.0,),
                                      Text('${order.count}'),
                                      SizedBox(width: 2.0,),
                                      InkWell(
                                        onTap: (){ //increase
                                          order.count += 1;
                                          setState(() {

                                          });
                                        },
                                        child: Container(
                                            padding: EdgeInsets.all(5.0),
//                                    color: appColor,
                                            child: Icon(Icons.expand_less,color: appColor,size: 20.0,)

                                        ),
                                      ),
                                    ],
                                  ),
//                                    Flexible(child: Container(),),
                                  InkWell(
                                    onTap: (){ //decrease
                                      orderItems.remove(order);
                                      setState(() {

                                      });
                                    },
                                    child: Container(
                                        padding: EdgeInsets.all(5.0),
//                                    color: appColor,
                                        child: Icon(Icons.delete_outline,color: appColor,size: 20.0,)

                                    ),
                                  ),

                                ],
                                crossAxisAlignment: CrossAxisAlignment.end,
                              )
                            ],
                          ),
                        ),
                      );
                    }).toList(),
                  ),
                  SizedBox(height: 10.0,),
                  Text('الاجمالي: ${totalPrice} ريال'),
                  SizedBox(height: 10.0,),
                  Container(
                    padding: const EdgeInsets.all(8.0),
                    width: double.infinity,
                    child: FlatButton(
                      onPressed: (){
                        if(orderItems.length > 0){
                          OrderDao dao = OrderDao();
                          dao.requestOrder(orderRestaurant.id, user_id, orderItems, (){
                            orderItems.clear();
                            setState(() {

                            });
                            Toast.show('تم ارسال طلبك بنجاح',context , duration: 3, gravity:  Toast.BOTTOM);
                          });
                        }
                      },
                      child: Text('متابعة الطلب',style: TextStyle(color: Colors.white),),
                      color: appColor,

                    ),
                  )
                ],
              ),
              decoration: new BoxDecoration(
                  color: Colors.white.withOpacity(0.7),
                  borderRadius: new BorderRadius.all(const Radius.circular(8.0))),
            ) : Container(),
          ),
        )
        ),
      );
  }
}
