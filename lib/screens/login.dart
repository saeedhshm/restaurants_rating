import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:email_validator/email_validator.dart';

import 'package:restu_ratingflutter_app/utils/colors.dart';
import 'package:restu_ratingflutter_app/utils/statics.dart';

import 'package:restu_ratingflutter_app/models/user_dao.dart';
import 'package:restu_ratingflutter_app/models/user.dart';
import 'package:restu_ratingflutter_app/models/resturant.dart';

import 'package:restu_ratingflutter_app/reusable_widgets/app_scaffold.dart';
import 'package:restu_ratingflutter_app/reusable_widgets/edit_text.dart';

import 'all_resto.dart';
import 'forget_password.dart';
import 'register.dart';
import 'restaurant_info.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {


  final emailCtr = TextEditingController();
  final passwordCtrl = TextEditingController();
  final formKey = GlobalKey<FormState>();
  var sendingData = false;

  int _groupLGN = 1;

  bool isUser = true;


  @override
  Widget build(BuildContext context) {


    return AppScaffold(
      context,
      screenTitle: "تسجيل الدخول",
      container: Container(
//        color: Colors.yellow,
        width: MediaQuery.of(context).size.width,
        child: Padding(
          padding: const EdgeInsets.only( left: 50.0,right: 50.0),
          child: SingleChildScrollView(
//          crossAxisAlignment: CrossAxisAlignment.center,
           child: Column(
             children: <Widget>[
               SizedBox(height: 50.0,),
               Container(
//          width: 20.0,
                 child: CircleAvatar(
                   radius: 50.0,
                   backgroundImage: AssetImage("assets/images/logo.png"),
                 ),
               ),
               SizedBox(height: 20.0,),
               Form(
                 key: formKey,
                   child: Column(
                     children: <Widget>[
                       EditText(
                         hintText: isUser? "youremail@mail.com":'اسم المستخدم',
                         labelText: isUser? "البريد الالكتروني":'اسم المستخدم',
                         txtEditingCrtl: emailCtr,
                         validator: (String value){
                           if(value.isEmpty){
                             return "لاتترك هذا الحقل فارغاً";
                           }
//                           print("=========================+++++++++++ ${EmailValidator.Validate(value, true)}");
                           if(!EmailValidator.validate(value.trim()) && isUser){
                             return "من فضلك ادخل بريد الكتروني صحيح";
                           }
                            return null;
                         },
                       ),
                       SizedBox(height:1.0),
                       EditText(
                         hintText: "********",
                         labelText: "كلمة المرور",
                         secureText: true,
                         txtEditingCrtl: passwordCtrl,
                         validator: (String value){

                           if(value.isEmpty){
                             return "لاتترك هذا الحقل فارغاً";
                           }

                           return null;
                         },
                       ),
                       SizedBox(height: 1.0,),
                       Row(
                         children: <Widget>[
                           Radio(value: 1,materialTapTargetSize: MaterialTapTargetSize.shrinkWrap, groupValue: _groupLGN, onChanged: (value){
                             _groupLGN = value;
                             isUser = true;
                             setState(() {

                             });
                           }),
                           Text('عميل',style: TextStyle(fontSize: 13.0),),
                           Radio(value: 2,materialTapTargetSize: MaterialTapTargetSize.shrinkWrap, groupValue: _groupLGN, onChanged: (value){
                             _groupLGN = value;
                             isUser = false;
                             setState(() {

                             });
                           }),
                           Text('إدارة',style: TextStyle(fontSize: 13.0),),

                         ],
                       ),
                       isUser?SizedBox(height: 15.0,):SizedBox(height: 0.0,),
                       isUser?Container(
//                         color: Colors.red,
                         child: InkWell(
                           onTap: (){
                             Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=>ForgetPassword()));
                           },
                           child: Text("نسيت كلمة السر؟",style: TextStyle(decoration: TextDecoration.underline,color: appColor),),
                         ),
                       ):Container(),
                       SizedBox(height: 15.0,),
                       InkWell(
                         onTap: (){


                           if(formKey.currentState.validate()){
                             User user = User.login(email: emailCtr.text,password: passwordCtrl.text);

                             sendingData = true;
                             setState(() {

                             });
                             UserDao userDao = UserDao(user);

                             //start login as user

                             if(isUser) {
                               userDao.login((User user) {
                                 sendingData = false;

                                 if(user == null){
                                   Toast.show(
                                       'خطأ في البريد الإلكتروني او كلمة المرور',
                                       context, duration: 3,
                                       gravity: Toast.BOTTOM);
                                 }else if(user.isStopped){
                                   Toast.show('هذا الحساب تم ايقافه', context,
                                       duration: 3, gravity: Toast.BOTTOM);
                                 }else{
                                   Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
                                       AllRestos()), (Route<dynamic> route) => false);
                                 }

//                               sendingData = true;
                                 setState(() {

                                 });
                               }); //end login user
                             }else{
                               //start login as restaurant manager
                               userDao.loginManager((Restaurant restoManager){
                                 sendingData = false;

                                 if(restoManager == null){
                                   Toast.show(
                                       'خطأ في اسم المستخدم او كلمة المرور',
                                       context, duration: 3,
                                       gravity: Toast.BOTTOM);
                                 }else if(restoManager.endDate != null && restoManager.endDate.compareTo(DateTime.now()) < 0 ){
                                   Toast.show('هذا الحساب تم ايقافه', context,
                                       duration: 3, gravity: Toast.BOTTOM);
                                 }else{

                                   Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
                                       RestaurantInfo(restoManager,isManager: true,)), (Route<dynamic> route) => false);
//                                   Navigator.pushAndRemove(
//                                     context,
//                                     MaterialPageRoute(
//                                         builder: (context) => RestaurantInfo(restoManager,isManager: true,)),
////                                     ModalRoute.of(context).,
//                                   );
                                 }

//                               sendingData = true;
                                 setState(() {

                                 });
                               });
                             }
                           }


                         },
                         child: Container(
                           width: double.infinity,
                           decoration: BoxDecoration(
                               color: appColor,
                             borderRadius: BorderRadius.all(Radius.circular(8.0))
                           ),
                           height: 40.0,

                           child: Center(child:
                           sendingData ?
                           CircularProgressIndicator(valueColor:new AlwaysStoppedAnimation<Color>(Colors.white),) :
                           Text("دخول",style: TextStyle(color: Colors.white),)),
                         ),
                       ),
                       SizedBox(height:30.0),
                       isUser?InkWell(
                         onTap: (){

                           Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=>Registeration(null)));

                         },
                         child: Text("انشاء حساب",style: TextStyle(decoration: TextDecoration.underline,color: appColor),),
                       ):Container(),
                     ],
                   ),
               )
             ],
           ),

          ),
        ),
      ),
    );
  }
}
