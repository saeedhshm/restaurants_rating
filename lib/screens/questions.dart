import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import 'package:restu_ratingflutter_app/models/question.dart';

import 'package:restu_ratingflutter_app/reusable_widgets/app_scaffold.dart';
import 'package:restu_ratingflutter_app/reusable_widgets/edit_text.dart';

import 'package:restu_ratingflutter_app/utils/colors.dart';
import 'package:restu_ratingflutter_app/utils/statics.dart';
import 'package:toast/toast.dart';

class Questions extends StatefulWidget {
  final int restau_id;
  final int user_id;

  Questions(this.restau_id, this.user_id);

  @override
  _QuestionsState createState() => _QuestionsState(restau_id, this.user_id);
}

class _QuestionsState extends State<Questions> {
  Question question;
  QuestionsDao dao;

  final commentCtr = TextEditingController();
  final phoneCtr = TextEditingController();
  final nametCtr = TextEditingController();

  int _groupQ1 = 0;
  int _groupQ2 = 0;
  int _groupQ3 = 0;
  int _groupQ4 = 0;
  int _groupQ5 = 0;

  File _image;
  bool sendingData = false;

  final int restau_id;
  final int user_id;

  _QuestionsState(this.restau_id, this.user_id);

  Future getImage() async {}

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    dao = QuestionsDao();
    dao.getQuestions((questions) {
      question = questions;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    print("============>>>>>>>>>>>>>.... user_id $user_id");
    return AppScaffold(
      context,
      screenTitle: "اضف تقييم",
      isManager: user_id == -2,
      bg: 'assets/images/bg_rest.png',
      container: question == null
          ? Container(child: Center(child: CircularProgressIndicator()))
          : Container(
              width: double.infinity,
              padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Card(
                      child: Container(
                        padding: EdgeInsets.all(10.0),
                        width: double.infinity,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text('س١: ${question.questions['Question1']}؟'),
                            SizedBox(
                              height: 5.0,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
//                        crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Radio(
                                    value: 5,
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    groupValue: _groupQ1,
                                    onChanged: (value) {
                                      _groupQ1 = value;
                                      setState(() {});
                                    }),
                                Text(
                                  'ممتاز',
                                  style: TextStyle(fontSize: 13.0),
                                ),
                                Radio(
                                    value: 4,
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    groupValue: _groupQ1,
                                    onChanged: (value) {
                                      _groupQ1 = value;
                                      setState(() {});
                                    }),
                                Text('جيد', style: TextStyle(fontSize: 13.0)),
                                Radio(
                                    value: 2,
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    groupValue: _groupQ1,
                                    onChanged: (value) {
                                      _groupQ1 = value;
                                      setState(() {});
                                    }),
                                Text('مقبول', style: TextStyle(fontSize: 13.0)),
                                Radio(
                                    value: 1,
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    groupValue: _groupQ1,
                                    onChanged: (value) {
                                      _groupQ1 = value;
                                      setState(() {});
                                    }),
                                Text('سئ', style: TextStyle(fontSize: 13.0)),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    Card(
                      child: Container(
                        padding: EdgeInsets.all(10.0),
                        width: double.infinity,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text('س٢: ${question.questions['Question2']}؟'),
                            SizedBox(
                              height: 5.0,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
//                        crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Radio(
                                    value: 5,
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    groupValue: _groupQ2,
                                    onChanged: (value) {
                                      _groupQ2 = value;
                                      setState(() {});
                                    }),
                                Text('ممتاز', style: TextStyle(fontSize: 13.0)),
                                Radio(
                                    value: 4,
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    groupValue: _groupQ2,
                                    onChanged: (value) {
                                      _groupQ2 = value;
                                      setState(() {});
                                    }),
                                Text('جيد', style: TextStyle(fontSize: 13.0)),
                                Radio(
                                    value: 2,
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    groupValue: _groupQ2,
                                    onChanged: (value) {
                                      _groupQ2 = value;
                                      setState(() {});
                                    }),
                                Text('مقبول', style: TextStyle(fontSize: 13.0)),
                                Radio(
                                    value: 1,
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    groupValue: _groupQ2,
                                    onChanged: (value) {
                                      _groupQ2 = value;
                                      setState(() {});
                                    }),
                                Text('سئ', style: TextStyle(fontSize: 13.0)),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    Card(
                      child: Container(
                        padding: EdgeInsets.all(10.0),
                        width: double.infinity,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text('س٣: ${question.questions['Question3']}؟'),
                            SizedBox(
                              height: 5.0,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
//                        crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Radio(
                                    value: 5,
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    groupValue: _groupQ3,
                                    onChanged: (value) {
                                      _groupQ3 = value;
                                      setState(() {});
                                    }),
                                Text('ممتاز', style: TextStyle(fontSize: 13.0)),
                                Radio(
                                    value: 4,
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    groupValue: _groupQ3,
                                    onChanged: (value) {
                                      _groupQ3 = value;
                                      setState(() {});
                                    }),
                                Text('جيد', style: TextStyle(fontSize: 13.0)),
                                Radio(
                                    value: 2,
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    groupValue: _groupQ3,
                                    onChanged: (value) {
                                      _groupQ3 = value;
                                      setState(() {});
                                    }),
                                Text('مقبول', style: TextStyle(fontSize: 13.0)),
                                Radio(
                                    value: 1,
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    groupValue: _groupQ3,
                                    onChanged: (value) {
                                      _groupQ3 = value;
                                      setState(() {});
                                    }),
                                Text('سئ', style: TextStyle(fontSize: 13.0)),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    Card(
                      child: Container(
                        padding: EdgeInsets.all(10.0),
                        width: double.infinity,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text('س٤: ${question.questions['Question4']}؟'),
                            SizedBox(
                              height: 5.0,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
//                        crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Radio(
                                    value: 5,
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    groupValue: _groupQ4,
                                    onChanged: (value) {
                                      _groupQ4 = value;
                                      setState(() {});
                                    }),
                                Text('ممتاز', style: TextStyle(fontSize: 13.0)),
                                Radio(
                                    value: 4,
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    groupValue: _groupQ4,
                                    onChanged: (value) {
                                      _groupQ4 = value;
                                      setState(() {});
                                    }),
                                Text('جيد', style: TextStyle(fontSize: 13.0)),
                                Radio(
                                    value: 2,
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    groupValue: _groupQ4,
                                    onChanged: (value) {
                                      _groupQ4 = value;
                                      setState(() {});
                                    }),
                                Text('مقبول', style: TextStyle(fontSize: 13.0)),
                                Radio(
                                    value: 1,
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    groupValue: _groupQ4,
                                    onChanged: (value) {
                                      _groupQ4 = value;
                                      setState(() {});
                                    }),
                                Text('سئ', style: TextStyle(fontSize: 13.0)),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    Card(
                      child: Container(
                        padding: EdgeInsets.all(10.0),
                        width: double.infinity,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text('س٥: ${question.questions['Question5']}؟'),
                            SizedBox(
                              height: 5.0,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
//                        crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Radio(
                                    value: 5,
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    groupValue: _groupQ5,
                                    onChanged: (value) {
                                      _groupQ5 = value;
                                      setState(() {});
                                    }),
                                Text('ممتاز', style: TextStyle(fontSize: 13.0)),
                                Radio(
                                    value: 4,
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    groupValue: _groupQ5,
                                    onChanged: (value) {
                                      _groupQ5 = value;
                                      setState(() {});
                                    }),
                                Text('جيد', style: TextStyle(fontSize: 13.0)),
                                Radio(
                                    value: 2,
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    groupValue: _groupQ5,
                                    onChanged: (value) {
                                      _groupQ5 = value;
                                      setState(() {});
                                    }),
                                Text('مقبول', style: TextStyle(fontSize: 13.0)),
                                Radio(
                                    value: 1,
                                    materialTapTargetSize:
                                        MaterialTapTargetSize.shrinkWrap,
                                    groupValue: _groupQ5,
                                    onChanged: (value) {
                                      _groupQ5 = value;
                                      setState(() {});
                                    }),
                                Text('سئ', style: TextStyle(fontSize: 13.0)),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    (user_id == -2) ? Card(
                      child: Container(
                        padding:  EdgeInsets.all(8.0),
                        child: Column(
                          children: <Widget>[
//                            EditText(
//                              labelText: "اسم العميل",
//                              txtEditingCrtl: nametCtr,
//                            ),
//                            SizedBox(height: 5.0,),
                            EditText(
                              labelText: "جوال العميل",
                              txtEditingCrtl: phoneCtr,
                            ),
                          ],
                        ),
                      ),
                    ): Container(),
                    Card(
                      child: (user_id == -2) ?Container() : Container(
                        child: Column(
                          children: <Widget>[
                            Center(
                              child: _image == null
                                  ? Text('يجب رفع صورة الفاتورة لهذا المطعم')
                                  : Image.file(_image),
                            ),
                            FlatButton(
                              onPressed: () {
                                showDialog(
                                    context: context,
                                    builder: (_) => new AlertDialog(
//                                title: new Text(""),
//                                content: new Text("This is my content"),
                                          actions: <Widget>[
                                            Container(
                                                child: FlatButton(
                                                    onPressed: () async {
                                                      var image = await ImagePicker
                                                          .pickImage(
                                                              source:
                                                                  ImageSource
                                                                      .camera,
                                                              maxWidth: 1920.0,
                                                              maxHeight:
                                                                  1080.0);
                                                      Navigator.of(context)
                                                          .pop();
                                                      setState(() {
                                                        _image = image;
                                                      });
                                                    },
                                                    child: Text("الكاميرا"))),
//                                  (),
                                            Container(
                                                child: FlatButton(
                                                    onPressed: () async {
                                                      var image = await ImagePicker
                                                          .pickImage(
                                                              source:
                                                                  ImageSource
                                                                      .gallery,
                                                              maxWidth: 1920.0,
                                                              maxHeight:
                                                                  1080.0);
                                                      Navigator.of(context)
                                                          .pop();
                                                      setState(() {
                                                        _image = image;
                                                      });
                                                    },
                                                    child: Text("الصور"))),
                                          ],
                                        ));
                              },
                              child: Icon(
                                Icons.add_a_photo,
                                color: appColor,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Card(
                      child: Container(
                        padding: EdgeInsets.all(8.0),
                        child: EditText(
                          labelText: "ضع تعليق",
                          maxLine: 3,
                          txtEditingCrtl: commentCtr,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    InkWell(
                      onTap: () {
                        if (_groupQ1 == 0 ||
                            _groupQ2 == 0 ||
                            _groupQ3 == 0 ||
                            _groupQ4 == 0 ||
                            _groupQ5 == 0) {
                          Toast.show('يرجي الاجابة علي جميع الاسئلة', context,
                              duration: 3, gravity: Toast.BOTTOM);
                        } else if(nametCtr.text == "" && user_id == -2 && false) {
                          Toast.show('يرجي ادخال اسم العميل', context,
                              duration: 3, gravity: Toast.BOTTOM);
                        }else if (_image == null && user_id != -2) {
                          Toast.show('يرجي رفع صورة الفاتورة', context,
                              duration: 3, gravity: Toast.BOTTOM);
                        } else {
                          sendingData = true;
                          setState(() {});
                          var questions = [
                            _groupQ1,
                            _groupQ2,
                            _groupQ3,
                            _groupQ4,
                            _groupQ5
                          ];

                          if (user_id == -2) {
                            dao.localRate(_image,
                                questions, "$restau_id", commentCtr.text,nametCtr.text,phoneCtr.text,
                                (msg) {
                              Toast.show(msg, context,
                                  duration: 3, gravity: Toast.BOTTOM);
                              sendingData = false;
                              commentCtr.text = "";
                              nametCtr.text = "";
                              phoneCtr.text = "";
                              _image = null;
                              _groupQ5 = 0;
                              _groupQ4 = 0;
                              _groupQ3 = 0;
                              _groupQ2 = 0;
                              _groupQ1 = 0;
                              setState(() {});
                            });
                          } else {
                            dao.uploadInvoice(_image, (response) {
                              if (response == "تم تحميل الفاتورة") {
                                dao.rateRestaurant(questions, "$user_id",
                                    "$restau_id", commentCtr.text, (msg) {
                                  Toast.show(msg, context,
                                      duration: 3, gravity: Toast.BOTTOM);
                                  sendingData = false;
                                  _image = null;
                                  commentCtr.text = "";
                                  _groupQ5 = 0;
                                  _groupQ4 = 0;
                                  _groupQ3 = 0;
                                  _groupQ2 = 0;
                                  _groupQ1 = 0;
                                  setState(() {});
                                });
                              }
                            }); //user rate
                          }
                        }
                      },
                      child: Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                            color: appColor,
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0))),
                        height: 40.0,
                        child: Center(
                          child: sendingData
                              ? CircularProgressIndicator(
                                  valueColor: new AlwaysStoppedAnimation<Color>(
                                      Colors.white),
                                )
                              : Center(
                                  child: Text(
                                  "ارسال التقييم",
                                  style: TextStyle(color: Colors.white),
                                )),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
    );
  }
}
