import 'package:flutter/material.dart';

import 'package:restu_ratingflutter_app/utils/app_urls.dart';
import 'package:restu_ratingflutter_app/utils/colors.dart';
import 'package:restu_ratingflutter_app/utils/statics.dart';

import 'package:restu_ratingflutter_app/models/current_offers.dart';

import 'package:restu_ratingflutter_app/reusable_widgets/app_scaffold.dart';

import 'offer_price.dart';

class CurrentOffers extends StatefulWidget {
  @override
  _CurrentOffersState createState() => _CurrentOffersState();
}

class _CurrentOffersState extends State<CurrentOffers> {

  List<CurrentOffer> currentOffers;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    CurrentOfferDao dao = CurrentOfferDao();
    dao.getCurrentOffers((currentOffers){
      this.currentOffers = currentOffers;
      setState(() {

      });
    });

  }
  @override
  Widget build(BuildContext context) {
    return AppScaffold(context,
      screenTitle: "العروض",
      bg: "assets/images/bg_dark.png",
      container: Container(
       child: currentOffers == null ?
        Center(child: CircularProgressIndicator(),)
        :Container(
         child: ListView.builder(
           itemCount: currentOffers.length,
             itemBuilder: (context,index) {
             CurrentOffer offer = currentOffers[index];
               return InkWell(
                 onTap: (){
                   Navigator.push(
                     context,
                     MaterialPageRoute(builder: (context) => OfferPriceScreen(offer.OfferID,offer.OfferName)),
                   );
                 },
                 child: Padding(
                   padding: const EdgeInsets.all(8.0),
                   child: Card(
                     child: Row(
                       crossAxisAlignment: CrossAxisAlignment.start,
                       children: <Widget>[
                         Container(
                           color: appColor,
                           width: 100.0,
                           height: 100.0,
                           child:offer.OfferPic != null ? Container(

//                         color: Colors.blue,
                               decoration: BoxDecoration(

//                        color: Colors.yellow,
                                 image: DecorationImage(
                                   image: NetworkImage("$imgOffer/${currentOffers[index].OfferPic}"),
                                   fit: BoxFit.fill,

                                   // ...
                                 ),
                                 // ...
                               )
                           ):Container(),
                         ),
                       SizedBox(width: 8.0,),

                       Expanded(
                         child: Column(
                           crossAxisAlignment: CrossAxisAlignment.start,
//                         mainAxisAlignment: MainAxisAlignment.spaceAround,
                             children: <Widget>[
                               Padding(
                                 padding: const EdgeInsets.all(8.0),
                                 child: Container(
//                                   color: Colors.blue,
                                     child: Center(child: Text('${offer.OfferName}',style: TextStyle(fontSize: 18.0,color: appColor),))
                                 ),
                               ),
                               Row(
                                 children: <Widget>[
                                   Column(
                                     crossAxisAlignment: CrossAxisAlignment.start,

                                     children: <Widget>[
                                       Text('اسم المطعم'),
                                       Text('بداية العرض'),
                                       Text('انتهاء العرض'),

                                     ],
                                   ),
                                   SizedBox(width: 10.0,),
                                   Column(
                                     crossAxisAlignment: CrossAxisAlignment.start,

                                     children: <Widget>[
                                       Text(': ${offer.RestaurantName}'),
                                       Text(': ${dateTime(offer.SDate)}'),
                                       Text(': ${dateTime(offer.EDate)}')

                                     ],
                                   )
                                 ],
                               )


                             ],
                           ),
                       )
                       ],
                     ),
                   ),
                 ),
               );
             }),
       ),
      ),
    );
  }
}
