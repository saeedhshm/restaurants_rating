import 'dart:async';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cached_network_image/cached_network_image.dart';



import 'package:restu_ratingflutter_app/utils/statics.dart';

import 'package:restu_ratingflutter_app/utils/app_urls.dart';
import 'package:restu_ratingflutter_app/utils/colors.dart';
import 'package:restu_ratingflutter_app/models/resturant.dart';
import 'package:restu_ratingflutter_app/models/recipe.dart';
import 'package:restu_ratingflutter_app/models/order.dart';

import 'package:restu_ratingflutter_app/reusable_widgets/app_scaffold.dart';
import 'package:toast/toast.dart';

import 'questions.dart';
import 'recipe_items.dart';

List<OrderItem> orderItems = List();
Set<Restaurant> restaus = Set();
Restaurant orderRestaurant;

class RestaurantInfo extends StatefulWidget {

  final Restaurant restaurant;
  final bool isManager;

  RestaurantInfo(this.restaurant,{this.isManager = false});

  @override
  _RestaurantInfoState createState() => _RestaurantInfoState(restaurant,isManager);
}

class _RestaurantInfoState extends State<RestaurantInfo> {

  final Restaurant restaurant;
  final bool isManager;
  List<RecipeGroup> recipeGroups = List();
  List<RestoImage> images = List();
  var _current = 0;

  Completer<GoogleMapController> _controller = Completer();



  _RestaurantInfoState(this.restaurant,this.isManager);

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }

    return result;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
//    print('+_+_+_+_+_+_+_+ ${restaurant.id}');
    RecipeDao recipeDao = RecipeDao();
    recipeDao.getRecipeGroup(restaurant.id, (recipeGroup){
      this.recipeGroups = recipeGroup;
      setState(() {

      });
    });

    RestoDao dao = RestoDao();
    dao.getRestoImages(restaurant.id, (imgs){
      images = imgs;
      setState(() {

      });
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Icon> stars = List();
    for (int i = 0;i<5; i++){
      if(i < restaurant.rate){
        stars.add(Icon(Icons.star,color: Colors.yellow,));
      }else{
        stars.add(Icon(Icons.star_border));
      }
    }

    List<Icon> lstars = List();
    for (int i = 0;i<5; i++){
      if(i < restaurant.lRate){
        lstars.add(Icon(Icons.star,color: Colors.yellow,));
      }else{
        lstars.add(Icon(Icons.star_border));
      }
    }


//    List<Icon> lStars = List();
//    for (int i = 0;i<5; i++){
//      if(i < restaurant.lRate){
//        lStars.add(Icon(Icons.star,color: Colors.yellow,));
//      }else{
//        lStars.add(Icon(Icons.star_border));
//      }
//    }
//    var markerIdVal = restaurant.name;
//    final MarkerId markerId = MarkerId(markerIdVal);

print("============>>>>>>>>>>>>>.... ${dateTime(restaurant.startDate)}");
    return  AppScaffold(
      context,
        screenTitle: restaurant.name,
       isManager: isManager,
       bg: 'assets/images/bg_dark.png',
        container: Container(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                images.length > 0?makeCarouselSlider():
                Container(
                  color: Colors.white,
                  height: MediaQuery.of(context).size.width/1.75,
                  width:  MediaQuery.of(context).size.width,
                  child:  FadeInImage.assetNetwork(
                    placeholder: 'assets/images/logo.png',
                    image: "$imgUrl/${restaurant.mainImg}",
                    fit: BoxFit.fill,
                  ),

                ),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("التقييم",style: TextStyle(color: appColor),),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[ Row(
                          children: stars.map((icon)=>icon).toList(),
                        ),
                            FlatButton(
                              onPressed: () async {
                                SharedPreferences prefs = await SharedPreferences.getInstance();
                                int user_id = (prefs.getInt('user_id') ?? -1);
                                print('====== = == == = = == ====>>> $user_id');
                                if(isManager) {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => Questions(restaurant.id,-2)),
                                  );
                                }else if(user_id > -1){
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => Questions(restaurant.id,user_id)),
                                  );
                                }else{
                                  Toast.show('يجب تسجيل الدخول حتي تتمكن من اضافة تقييم',context , duration: 3, gravity:  Toast.BOTTOM);
                                }
                              },
                              child: Text('اضف تقييم',style: TextStyle(color: Colors.white),),
                              color: appColor,
                            )
                        ]
                      ),

                      Text("التقييم الداخلي",style: TextStyle(color: appColor),),
                      Row(
                        children: lstars.map((icon)=>icon).toList(),
                      ),

                      SizedBox(height: 8.0,),
                      Container(
                        width: double.infinity,
//                        color: Colors.white,
                          decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.8),
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(8.0),
                                  topRight: Radius.circular( 8.0)
                              )
                          ),
                          child:
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text("العنوان",style: TextStyle(color: appColor),),
                            Text(restaurant.address),

                      SizedBox(height: 8.0,),

                      Text("الجوال",style: TextStyle(color: appColor),),
                      Text(restaurant.mobile),

                      SizedBox(height: 8.0,),

                      Text("الهاتف",style: TextStyle(color: appColor),),
                      Text(restaurant.tel),

                      SizedBox(height: 8.0,),

                      Text("تاريخ الانضمام",style: TextStyle(color: appColor),),
                      Text(dateTime(restaurant.startDate)),

                      SizedBox(height: 8.0,),

                      Text("ملاحظات",style: TextStyle(color: appColor),),
                      Text(restaurant.note != null ? restaurant.note : "لا توجد ملاحظات"),
                          ],
                        ),
                      )),

                      SizedBox(height: 10.0,),

                      Container(
                        color: Colors.white.withOpacity(0.8),
                        width: double.infinity,
                        child: Padding(
                            padding: EdgeInsets.all(8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text("اقسام المنيو",style: TextStyle(color: appColor),),
                              SizedBox(height: 8.0,),
                              Column(

                                children: recipeGroups.map((RecipeGroup recGroup){
                                  return InkWell(
                                    onTap: (){
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(builder: (context) => RecipeItems(recGroup,restaurant)),
                                      );
                                    },
                                    child: Card(
                                      child: Padding(
                                          padding: EdgeInsets.all(8.0),
                                        child: Row(
                                          children: <Widget>[
                                            Container(
                                              padding: EdgeInsets.all(0.5),
                                              color:appColor,
                                              child: FadeInImage.assetNetwork(
                                                placeholder: 'assets/images/logo.png',
                                                image: "$recipeGroupImg/${recGroup.RecipeGroupPic}",
                                                fit: BoxFit.fill,
                                                height: 35.0,
                                                width: 50.0,
                                              ),
                                            ),
                                            SizedBox(width: 10.0,),
                                            Text(recGroup.RecipeGroupName)
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                }).toList(),
                              )
                            ],
                          ),
                        ),
                      ),

                      SizedBox(height: 10.0,),

                  Container(
                    width: double.infinity,
//                        color: Colors.white,
                    decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.8),
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(8.0),
                            bottomRight: Radius.circular( 8.0)
                        )
                    ),
                    child:
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                      Text("الموقع علي الخريطة",style: TextStyle(color: appColor),),
                      SizedBox(height: 5.0,),
                      Container(
                        height: MediaQuery.of(context).size.height/3.2,
                        width: double.infinity,
                        child: GoogleMap(
                          myLocationEnabled: true,
                          mapType: MapType.normal,
                          markers: Set<Marker>.of(<Marker>[
                            new Marker(markerId: MarkerId('${restaurant.id}'),

                              position: LatLng(restaurant.lat, restaurant.long)
                            )
                          ]),
                          initialCameraPosition: CameraPosition(
                            target: LatLng(restaurant.lat, restaurant.long),
                            zoom: 13.4746,
                          ),
                          onMapCreated: (GoogleMapController controller) {
                            _controller.complete(controller);
                          },
                        ),
                      ),//end of map

                        ],
                      ),
                    )),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      );
  }


  Widget makeCarouselSlider(){
    return Stack(

        children: [
          Container(
            height: MediaQuery.of(context).size.width/1.75,
            width:  MediaQuery.of(context).size.width,
            child: CarouselSlider(
              autoPlay: true,
              viewportFraction: 1.0,
              aspectRatio: 0.5,
              onPageChanged: (index){
                _current = index;
                setState(() {

                });
              },
              items: images.map(
                    (url) {
                      var imgUrl = '$restoImgsUrl/${url.FileName}';
//                      print('######## ${}');
                  return Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.symmetric(horizontal: 3.0),
                      decoration: BoxDecoration(
                          color: Colors.black.withOpacity(0.5)
                      ),
                      child: CachedNetworkImage(
                        imageUrl: imgUrl,
                        placeholder: (context, url) => Center(child: new CircularProgressIndicator()),
                        errorWidget: (context, url, error) => new Icon(Icons.error),
//                        width: double.infinity,
//                        height: double.infinity,
                        fit: BoxFit.fill,
                      )
                  );
                },
              ).toList(),
            ),
          ),
          Positioned(
              bottom: 0.0,
              left: 0.0,
              right: 0.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: map<Widget>(images, (index, url) {
                  return Container(
                    width: 8.0,
                    height: 8.0,
                    margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: _current == index ? appColor : Colors.white
                    ),
                  );
                }),
              )
          )
        ]
    );
  }
}
