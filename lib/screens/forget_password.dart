import 'package:flutter/material.dart';

import 'package:email_validator/email_validator.dart';

import 'package:restu_ratingflutter_app/utils/colors.dart';

import 'package:restu_ratingflutter_app/models/user_dao.dart';
import 'package:restu_ratingflutter_app/models/user.dart';

import 'package:restu_ratingflutter_app/reusable_widgets/app_scaffold.dart';
import 'package:restu_ratingflutter_app/reusable_widgets/edit_text.dart';
import 'package:toast/toast.dart';

class ForgetPassword extends StatefulWidget {
  @override
  _ForgetPasswordState createState() => _ForgetPasswordState();
}

class _ForgetPasswordState extends State<ForgetPassword> {


  final emailCtr = TextEditingController();
  final passwordCtrl = TextEditingController();
  final formKey = GlobalKey<FormState>();
  var sendingData = false;

//  final

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      context,
      screenTitle: "استعادة كلمة المرور",
      container: Container(
//        color: Colors.yellow,
        width: MediaQuery.of(context).size.width,
        child: Padding(
          padding: const EdgeInsets.only( left: 50.0,right: 50.0),
          child: SingleChildScrollView(
//          crossAxisAlignment: CrossAxisAlignment.center,
           child: Column(
             children: <Widget>[
               SizedBox(height: 50.0,),
               Container(
//          width: 20.0,
                 child: Column(
                   children: <Widget>[
                     Text("يرجي كتابة البريد الالكتروني",style: TextStyle(fontSize: 18.0,color: appColor),),
                     Text("ليتم ارسال كلمة المرور اليك", style: TextStyle(fontSize: 18.0,color: appColor),)
                   ],
                 )
               ),
               SizedBox(height: 20.0,),
               Form(
                 key: formKey,
                   child: Column(
                     children: <Widget>[
                       EditText(
                         hintText: "youremail@mail.com",
                         labelText: "البريد الالكتروني",
                         txtEditingCrtl: emailCtr,
                         validator: (String value){
                           if(value.isEmpty){
                             return "لاتترك هذا الحقل فارغاً";
                           }
//                           print("=========================+++++++++++ ${EmailValidator.Validate(value, true)}");
                           if(!EmailValidator.validate(value.trim())){
                             return "من فضلك ادخل بريد الكتروني صحيح";
                           }
                         },
                       ),

                       SizedBox(height: 15.0,),
                       InkWell(
                         onTap: (){


                           if(formKey.currentState.validate()){
                             sendingData = true;
                             setState(() {
                             });
                             UserDao dao = UserDao(null);
                             dao.retrievePassword(emailCtr.text, (msg){
                               Toast.show(msg,context , duration: 3, gravity:  Toast.BOTTOM);
                               sendingData = false;
                               setState(() {
                               });
                             });
                           }


                         },
                         child: Container(
                           width: double.infinity,
                           decoration: BoxDecoration(
                               color: appColor,
                             borderRadius: BorderRadius.all(Radius.circular(8.0))
                           ),
                           height: 40.0,

                           child:  Center(
                             child: sendingData ?
                             CircularProgressIndicator(valueColor:new AlwaysStoppedAnimation<Color>(Colors.white),) :
                             Center(child: Text("ارسال",style: TextStyle(color: Colors.white),)),
                           ),
                         ),
                       ),
                       SizedBox(height:30.0),

                     ],
                   ),
               )
             ],
           ),

          ),
        ),
      ),
    );
  }
}
