import 'package:flutter/material.dart';

import 'package:restu_ratingflutter_app/reusable_widgets/app_scaffold.dart';

import 'package:restu_ratingflutter_app/utils/colors.dart';
import 'package:restu_ratingflutter_app/utils/app_urls.dart';

import 'package:restu_ratingflutter_app/models/offer_price.dart';

class OfferPriceScreen extends StatefulWidget {

  final int offerId;
  final String offerName;

  OfferPriceScreen(this.offerId,this.offerName);
  @override
  _OfferPriceScreenState createState() => _OfferPriceScreenState(offerId,this.offerName);
}

class _OfferPriceScreenState extends State<OfferPriceScreen> {

  final int offerId;
  final String offerName;

  List<OfferPrice> offers ;

  _OfferPriceScreenState(this.offerId,this.offerName);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    OfferPriceDao dao = OfferPriceDao();
    dao.getAllOfferPrice(offerId, (offers){
        this.offers = offers;
        setState(() {

        });
    });
  }
  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      context,
      screenTitle: offerName,
      bg: 'assets/images/bg_dark.png',
      container: Container(
        child: offers == null ?
        Center(child: CircularProgressIndicator(),)
            : offers.length == 0? Center(child: Text('لا توجد عروض متاحة',style: TextStyle(
          fontSize: 18.0,color: appColor,fontWeight: FontWeight.bold
        ),),) :Container(
          child: ListView.builder(
            itemCount: offers.length,
              itemBuilder: (context,index){
              OfferPrice offer = offers[index];
              print('----------------> $imgOffer${offer.RecipePic}');
              return Card(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: double.infinity,
                        height: 150.0,
//                        color: appColor,
                          child: Container(

//                         color: Colors.blue,
                          child: FadeInImage.assetNetwork(
                              placeholder: 'assets/images/logo.png',
                              image:"$imgOffer${offer.RecipePic}",
//                            fit: BoxFit.fill,
                          ),
                              decoration: BoxDecoration(

//                        color: Colors.yellow,
                                image: DecorationImage(
                                  image: NetworkImage("$imgOffer${offer.RecipePic}"),
                                  fit: BoxFit.fill,

                                  // ...
                                ),
                                // ...
                              )
                          )
                      ),
                      SizedBox(height: 8.0,),
                      Text(offer.RecipeName),
                      SizedBox(height: 8.0,),
                      Row(
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text('السعر قبل العرض'),
                              Text('السعر بعد العرض'),
                              Text('نسبة الخصم')
                            ],
                          ),
                          SizedBox(width: 10.0,),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(': ${offer.Price}'),
                              Text(': ${offer.offerPrice}'),
                              Text(': ${offer.Priceratio}'),

                            ],
                          ),

                        ],
                      )

                      
                    ],
                  ),
                ),
              );

          }),
        ),
      ),
    );
  }
}
