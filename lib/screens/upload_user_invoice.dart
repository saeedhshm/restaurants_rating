import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


import 'package:restu_ratingflutter_app/models/resturant.dart';

import 'package:restu_ratingflutter_app/reusable_widgets/image.dart';
import 'package:restu_ratingflutter_app/reusable_widgets/edit_text.dart';
import 'package:restu_ratingflutter_app/utils/statics.dart';

import 'package:restu_ratingflutter_app/utils/styles.dart' as styles;
import 'package:restu_ratingflutter_app/utils/colors.dart';
import 'package:toast/toast.dart';

import 'all_resto.dart';

class UploadInvoice extends StatefulWidget {
  @override
  _UploadInvoiceState createState() => _UploadInvoiceState();
}

class _UploadInvoiceState extends State<UploadInvoice> {
  File _image;
  bool sendingData = false;
  Restaurant _selectedRestaurant;
  final priceCtr = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'رفع فاتورة الشراء',
            style: styles.appBarTextStyle,
          ),
        ),
        body: Container(
            height: double.infinity,
//            width: double.infinity,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/bg_dark.png"),
                fit: BoxFit.cover,
              ),
            ),
            child: Stack(
              children: <Widget>[
                SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(18.0),
                    child: Column(
                      children: <Widget>[
                        Card(
                          color: Colors.white.withOpacity(0.5),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
//                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                      border: Border.all(color: appColor),
                                    borderRadius: BorderRadius.all(Radius.circular(5.0))
                                  ),
                                  
                                  child: Padding(
                                    padding: const EdgeInsets.only(right:8.0,bottom: 4.0,top: 4.0),
                                    child: DropdownButton<Restaurant>(
                                      iconSize: 24,
                                      elevation: 16,
                                      isExpanded: true,
                                      style: TextStyle(color: appColor),
//                                      underline: Container(
//                                        width: double.infinity,
//                                        padding: EdgeInsets.all(20.0),
//                                        height: 0,
//                                        color: appColor,
//                                      ),
                                      hint: Text(
                                        'اختر مطعم',
                                        style: TextStyle(
                                            fontSize: 18.0,
                                            fontFamily: "CAREEM"),
                                      ), // Not necessary for Option 1
                                      value: _selectedRestaurant,
                                      onChanged: (newValue) {
                                        setState(() {
                                          _selectedRestaurant = newValue;
                                        });
                                      },
                                      items: allRestaus
                                          .map<DropdownMenuItem<Restaurant>>(
                                              (Restaurant value) {
                                        return DropdownMenuItem<Restaurant>(
                                          value: value,
                                          child: Text(
                                            value.name,
                                            style: TextStyle(
                                                fontSize: 16.0,
                                                fontFamily: "CAREEM"),
                                          ),
                                        );
                                      }).toList(),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 10.0,
                                ),
                                Container(
//                      width: double.infinity,
                                  child: TextField(
                                    controller: priceCtr,
//
                                    decoration: InputDecoration(
                                        enabledBorder: OutlineInputBorder(
                                          //0.0 produces a thin "hairline" border
                                          borderSide: BorderSide(
                                              color: appColor, width: 1.0),
//              borderRadius: BorderRadius.all(Radius.circular(50.0))
                                        ),
                                        border: const OutlineInputBorder(),
                                        hintText: '100.00',
                                        labelText: 'ادخل قيمة الفاتورة'),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 0.0,
                        ),
                        Card(
                          color: Colors.white.withOpacity(0.5),
                          child: Container(
                            child: Column(
                              children: <Widget>[
                                Center(
                                  child: _image == null
                                      ? Text('يرجي رفع صورة الفاتورة')
                                      : Image.file(_image),
                                ),
                                FlatButton(
                                  onPressed: () {
                                    chooseImage(context, (image) {
                                      _image = image;
                                      setState(() {});
                                    });
                                  },
                                  child: Icon(
                                    Icons.add_a_photo,
                                    color: appColor,
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 8.0,
                        ),
                        SizedBox(
                          width: double.infinity,
                          child: FlatButton(
                            padding: EdgeInsets.all(12.0),
                            onPressed: () {

                              if(_selectedRestaurant == null){
                                Toast.show('يرجي اختيار احد المطاعم', context);
                                return;
                              }
                              if(priceCtr.text == null || priceCtr.text == ""){
                                Toast.show('يرجي ادخال قيمة الفاتورة', context);
                                return;
                              }
                              if(_image == null){
                                Toast.show('يرجي رفع صورة الفاتورة', context);
                                return;
                              }
                              sendingData = true;
                              setState(() {});

                              RestoDao dao = RestoDao();
                              dao.uploadUserInvoice(_image, user_id, _selectedRestaurant.id, priceCtr.text, (map){
                                print("_+_+_+_+_+_+_+_+_+_+ $map");
                                sendingData = false;
                                setState(() {
                                  Toast.show('$map', context);
                                  priceCtr.text = "";
                                  _selectedRestaurant = null;
                                  _image = null;
                                });
                              });
                            },
                            child: Text(
                              'ارسال الفاتورة',
                              style: TextStyle(color: Colors.white),
                            ),
                            color: appColor,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                sendingData
                    ? Container(
                        width: double.infinity,
                        height: double.infinity,
                        color: Colors.black.withOpacity(0.4),
                        child: Center(
                          child: activityIndicator(),
                        ),
                      )
                    : Container()
              ],
            )));
  }
}
