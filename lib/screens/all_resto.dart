import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:restu_ratingflutter_app/utils/app_urls.dart';
import 'package:restu_ratingflutter_app/utils/colors.dart';
import 'package:restu_ratingflutter_app/utils/statics.dart';


import 'package:restu_ratingflutter_app/reusable_widgets/app_scaffold.dart';
import 'package:restu_ratingflutter_app/reusable_widgets/edit_text.dart';

import 'package:restu_ratingflutter_app/models/resturant.dart';
import 'package:restu_ratingflutter_app/models/notif_message.dart';
import 'package:restu_ratingflutter_app/models/kitchen_type.dart';

import 'package:restu_ratingflutter_app/screens/restaurant_info.dart';

import 'questions.dart';

List<Restaurant> allRestaus = List();

class AllRestos extends StatefulWidget {
  @override
  _AllRestosState createState() => _AllRestosState();
}

class _AllRestosState extends State<AllRestos> {

  List<Restaurant> restaus;
  KitchenType _selectedRestaurant;
  List<KitchenType> _kitchenTypes;
  final searchTxtCtrl = TextEditingController();

  bool isStarSelected = false;
  bool locationOn = false;

//  int user_id = -1;

  _AllRestosState(){
    _awaitFor();
  }
 
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    var dao = KitchenTypeDao();
    dao.getKitchenTypes((kitchenTypes){

      this._kitchenTypes = kitchenTypes;
      this._kitchenTypes.insert(0, KitchenType(-1,'الكل'));
      setState(() {

      });
    });

    _getAllRestaurants(typeId: -1);

    _awaitFor().then((v){
      print("=-===============->> $user_id");
      setState(() {
        if(user_id != -1) {
          NotifMessageDao notifMessageDao = NotifMessageDao();
          notifMessageDao.getNofs(user_id, (List notifs) {
            if(notifs.length > 0)
              _showNotifations(notifs);
          });
        }
      });
    });




  }

  Future<void> _awaitFor() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    user_id = (prefs.getInt('user_id') ?? -1);
    print("=-===============->> await $user_id");

  }
  @override
  Widget build(BuildContext context) {

    return AppScaffold( context,
      screenTitle: "المطاعم والمقاهي ",
      bg: 'assets/images/bg_dark.png',
      isAllResto: true,
      actions: <Widget>[

//        InkWell(
//          onTap: (){
//            locationOn = !locationOn;
//
//            if(locationOn){
//              restaus.sort((a , b){
//                return (b.rate).compareTo(a.rate);
//              });
//            }else{
//              restaus.clear();
//              restaus.addAll(allRestaus);
//            }
//
//            setState(() {
//
//            });
//          },
//          child:locationOn ? Icon(Icons.location_on) : Icon(Icons.location_off),
//        ),
      InkWell(
        onTap: (){
          isStarSelected = !isStarSelected;

          if(isStarSelected){
            restaus.sort((a , b){
              return (b.rate).compareTo(a.rate);
            });
          }else{
            restaus.clear();
            restaus.addAll(allRestaus);
          }
          setState(() {

          });
        },
        child: isStarSelected? Icon(Icons.star,color: Colors.yellow,) : Icon(Icons.star_border),
      ),
        InkWell(
          onTap: _filterAction,
          child: Icon(Icons.filter_list),
        )
      ],
      container: Container(

        height: double.infinity,
//            width: double.infinity,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg_dark.png"),
            fit: BoxFit.cover,
          ),
        ),

        padding: EdgeInsets.symmetric(horizontal: 16.0,vertical: 0.0),
        child: Center(
          child: restaus == null ?
              CircularProgressIndicator() :

          Column(
            children: <Widget>[
              Container(
                height: 50.0,
                width: MediaQuery.of(context).size.width,
//                color: appColor,
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              child: Row(
                children: <Widget>[
                  Expanded(child: Padding(
                    padding: const EdgeInsets.all(7.0),
                    child: Container(
                      width: double.infinity,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                          border: Border.all(color: appColor)),
                      child: Padding(
                        padding: const EdgeInsets.only(right:8.0),
                        child: SizedBox(
                          width: double.infinity,
                          child: _kitchenTypes == null ? Padding(
                            padding: EdgeInsets.all(5.0),
                            child: Center(child: CircularProgressIndicator(),),
                          ) : DropdownButton<KitchenType>(
                            iconSize: 24,
                            elevation: 16,
                            isExpanded: true,
                            style: TextStyle(color: appColor),
//                                      underline: Container(
//                                        width: double.infinity,
//                                        padding: EdgeInsets.all(20.0),
//                                        height: 0,
//                                        color: appColor,
//                                      ),
                            hint: Text(
                              'اختر نوع المطعم',
                              style: TextStyle(
                                  fontSize: 15.0,
                                  fontFamily: "CAREEM"),
                            ), // Not necessary for Option 1
                            value: _selectedRestaurant,
                            onChanged: (newValue) {
                              setState(() {
                                _selectedRestaurant = newValue;
                                _getAllRestaurants(typeId: newValue.typeId);
                              });
                            },
                            items: _kitchenTypes
                                .map<DropdownMenuItem<KitchenType>>(
                                    (KitchenType value) {
                                  return DropdownMenuItem<KitchenType>(
                                    value: value,
                                    child: Text(
                                      value.typeName,
                                      style: TextStyle(
                                          fontSize: 16.0,
                                          fontFamily: "CAREEM"),
                                    ),
                                  );
                                }).toList(),
                          ),
                        ),
                      ),
                    ),
                  ),),
//                  SizedBox(width: 10.0,),
                  Expanded(child: Padding(
                    padding: const EdgeInsets.all(6.0),
                    child: TextField(
                      controller: searchTxtCtrl,
                      onChanged: _searchInList,
                      decoration: new InputDecoration(
                          enabledBorder: OutlineInputBorder(
                            //0.0 produces a thin "hairline" border
                            borderSide: BorderSide(
                                color: appColor, width: 1.0),
//              borderRadius: BorderRadius.all(Radius.circular(50.0))
                          ),
                          border: const OutlineInputBorder(),
                          hintText: 'بحث'
                      ),
                    ),
                  ))
                ],
              ),
              ),
              SizedBox(height: 8.0,),
              Flexible(
                child: Container(
                  width: double.infinity,
//                height: double.infinity,
//                  color: Colors.blue,
                  child: ListView.builder(
                    itemCount: restaus.length,
                    itemBuilder: (context,index){
                      List<Icon> stars = List();
                      for (int i = 0;i<5; i++){
                        if(i < restaus[index].rate){
                          stars.add(Icon(Icons.star,color: Colors.yellow,size: 18.0,));
                        }else{
                          stars.add(Icon(Icons.star_border,size: 18.0));
                        }
                      }
                      return new Card(

                        child: InkWell(
                          onTap: (){
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => RestaurantInfo(restaus[index])),
                            );
                          },
                          child: Container(
                            padding: EdgeInsets.all(8.0),
//                color: Colors.yellow,
                            child: Row(
                              children: <Widget>[

                                CircleAvatar(
                                  child: ClipOval(child:Container(
                                    height: MediaQuery.of(context).size.height,
                                    width:  MediaQuery.of(context).size.width,
                                    child:  CachedNetworkImage(
                                      imageUrl: "$imgUrl/${restaus[index].mainImg}",
//                                      placeholder: (context, url) => Center(child: new CircularProgressIndicator()),
                                      errorWidget: (context, url, error) => new Icon(Icons.error),
                                      fit: BoxFit.fill,
                                    )

                                  ),),
                                  backgroundColor: appColor.withOpacity(0.5),
                                  radius: 30.0,


                                ),
                                SizedBox(width: 8.0,),
                                Flexible(
                                  child: Container(
//                      color: Colors.red,
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Text('${restaus[index].name}',style: TextStyle(color: appColor,fontSize: 13.0),),
                                            InkWell(
                                              onTap: ()  {

                                                print('====== = == == = = == ====>>> $user_id');
                                                if(user_id > -1){
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute(builder: (context) => Questions(restaus[index].id,user_id)),
                                                  );
                                                }else{
                                                  Toast.show('يجب تسجيل الدخول حتي تتمكن من اضافة تقييم',context , duration: 3, gravity:  Toast.BOTTOM);
                                                }
                                              },
                                              child: Container(
//                                    onPressed: (){},
                                                padding: EdgeInsets.only(left: 8.0,right: 8.0,top: 8.0,bottom: 10.0),
                                                child: Text("اضف تقييم", style: TextStyle(color: Colors.white,fontSize: 13.0,fontWeight: FontWeight.w400),),
                                                color: appColor,
                                              ),
                                            )
                                          ],
                                        ),
//                              Text(restaus[index].address),
                                        Row(
                                          children: stars.map((icon)=>icon).toList(),
                                        )
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ),
              )
            ],
          )
        ),
      ),
    );
  }

//  _filterAction(){
//      print("filter action hhhhh");
//  }

  Future<void> _showNotifations(List<NotifMessage> msgs){
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context){
        return AlertDialog(
          title: Text(''),
          content: SingleChildScrollView(
            child: ListBody(
              children: msgs.map((NotifMessage notifMsg){
                return Card(
                  child: Container(
                    padding: EdgeInsets.all(8.0),
                    child: Column(
                      children: <Widget>[
                        Text('الجائزة : ${notifMsg.Prize}'),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,

                              children: <Widget>[
                                Text('اسم العميل'),
                                Text('المطعم'),
                                Text('النقاط المستبدلة'),
                                Text('الرسالة'),
                                Text('تاريخ الارسال')
                              ],
                            ),
                            SizedBox(width: 10.0,),
                            Flexible(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(notifMsg.ClientName),
                                  Text(notifMsg.RestaurantName),
                                  Text('${notifMsg.Points}'),
                                  Text(notifMsg.Mssge),
                                  Text(dateTime(notifMsg.Sendtime))
                                ],
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              }).toList(),
            ),
          ),
          actions: <Widget>[
            FlatButton(
              onPressed: (){
                Navigator.of(context).pop();
                setState(() {

                });
              },
              child: Text("موافق"),
            )
          ],
        );
      }
    );
  }

  Future<void> _filterAction() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('تصنيف بـ'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                FlatButton(
                  onPressed: (){

                    restaus.clear();
                    for(Restaurant restu in allRestaus){
//                      if(restu.name.contains(name.trim())){
                        restaus.add(restu);
//                      }
                    }
                    setState(() {

                    });
                    Navigator.of(context).pop();
                  },
                  child: Text('الكل'),
                ),
                FlatButton(
                  onPressed: (){

                    restaus.clear();
                    for(Restaurant restu in allRestaus){
                      if(restu.typeName.contains('مطاعم')){
                        restaus.add(restu);
                      }
                    }
                    setState(() {

                    });
                    Navigator.of(context).pop();
                  },
                  child: Text('مطاعم'),
                ),
                FlatButton(
                  onPressed: (){

                    restaus.clear();
                    for(Restaurant restu in allRestaus){
                      if(restu.typeName.contains('كافية')){
                        restaus.add(restu);
                      }
                    }
                    setState(() {

                    });
                    Navigator.of(context).pop();
                  },
                  child: Text('مقاهي'),
                ),
              ],
            ),
          ),
//          actions: <Widget>[
//            FlatButton(
//              child: Text('Regret'),
//              onPressed: () {
//                Navigator.of(context).pop();
//              },
//            ),
//          ],
        );
      },
    );
  }

  _searchInList(String name){
//     name.trim();
    restaus.clear();
    for(Restaurant restu in allRestaus){
      if(restu.name.contains(name.trim())){
        restaus.add(restu);
      }
    }
    setState(() {

    });
  }

  _getAllRestaurants({@required int typeId}){

    RestoDao dao = RestoDao();
    dao.getAllRestaus("$typeId", (paringRestaus){
      this.restaus = List();
      allRestaus.clear();
      for(Restaurant restu in paringRestaus){
        if((restu.endDate == null || restu.endDate.compareTo(DateTime.now()) >= 0) && restu.showRestau  ) {
          this.restaus.add(restu);
          allRestaus.add(restu);

          searchTxtCtrl.text = "";

        }
      }
//      this.restaus = paringRestaus;
      setState(() {

      });
    });
  }
}
