import 'package:flutter/material.dart';

import 'package:restu_ratingflutter_app/models/user_dao.dart';
import 'package:restu_ratingflutter_app/models/user.dart';
import 'package:restu_ratingflutter_app/models/activity_log.dart';

import 'package:restu_ratingflutter_app/reusable_widgets/app_scaffold.dart';

import 'package:restu_ratingflutter_app/utils/colors.dart';
import 'package:restu_ratingflutter_app/utils/statics.dart';

import 'register.dart';

class UserProfile extends StatefulWidget {
  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  User _user;
  List<UserActivity> activities = List();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    UserDao userDao = UserDao(null);
    userDao.getUserInfo((user) {
      _user = user;
      setState(() {});
        UserActivityDao dao = UserActivityDao();
        dao.getUserActivities(userId: _user.userId,restauId: -1,completion: (acts){
          this.activities = acts;
          setState(() {

          });
        });
    });
  }

  @override
  Widget build(BuildContext context) {
    return AppScaffold(
      context,
      screenTitle: "الصفحة الشخصية",
//      bg: '',
      container: Container(
        child: _user == null
            ? Center(
                child: CircularProgressIndicator(
                  valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
                ),
              )
            : Container(
                padding: EdgeInsets.only(left: 16.0,right: 16.0,top: 10.0),
                child: ListView.builder(
                    itemCount: activities.length + 2, itemBuilder: (context, index) {
                      if(index == 0){
                        return getContainerInfo();
                      }else if(index == 1){
                        return Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Card(
                            color: appColor.withOpacity(0.5),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: <Widget>[
                                  Text('سجل النشاطات',style: TextStyle(color: Colors.white),),
                                ],
                              ),
                            ),
                          ),
                        );
                      }else{ //end of index 1
                        UserActivity activity = activities[(index-2)];
                        List<Icon> stars = List();
                        for (int i = 0;i<5; i++){
                          if(i < (activity.TotalRate.toInt())){
                            stars.add(Icon(Icons.star,color: Colors.yellow,size: 18.0,));
                          }else{
                            stars.add(Icon(Icons.star_border,size: 18.0));
                          }
                        }
                       return Padding(
                         padding: EdgeInsets.all(0.0),
                         child: Card(
                           elevation: 0.0,
                            color: Colors.transparent,
                           child: Container(
                             decoration: BoxDecoration(
                                 color: Colors.white.withOpacity(0.6),
                                 borderRadius: BorderRadius.all(Radius.circular(8.0))),

                             padding: EdgeInsets.all(8.0),
                             child: Column(
                               crossAxisAlignment: CrossAxisAlignment.start,
                               children: <Widget>[
                                 Text(activity.RestaurantName,style: TextStyle(color: appColor),),
                                 SizedBox(height: 5.0,),
                                 Row(
                                   children: stars.map((icon)=>icon).toList(),
                                 ),
                                 SizedBox(height: 6.0,),
                                 Text('التعليق :${activity.Comment}',style: TextStyle(fontSize: 12.0),),
                                 SizedBox(height: 5.0,),
                                 Text('تاريخ التقييم :${dateTime(activity.RateDate)}',style: TextStyle(fontSize: 12.0),),
                                 SizedBox(height: 5.0,),
//                                 Text('التقييم ${activity.Active ? 'مفعل' : 'غير مفعل'}',style: TextStyle(fontSize: 12.0,color: activity.Active?Colors.green : Colors.red),)
                               ],
                             ),
                           ),
                         ),
                       );
                      }
                }),
              ),
      ),
    );
  }

  Widget getContainerInfo() {
    return Container(
      padding: EdgeInsets.all(8.0),
      decoration: BoxDecoration(
          color: Colors.white.withOpacity(0.6),
          borderRadius: BorderRadius.all(Radius.circular(8.0))),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[

              //title column
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'اسم المستخدم :',
                    style: TextStyle(color: appColor),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text('رقم التليفون :', style: TextStyle(color: appColor)),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text('البريد الالكتروني :', style: TextStyle(color: appColor)),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text('تاريخ الميلاد :', style: TextStyle(color: appColor)),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text('تاريخ التسجيل :', style: TextStyle(color: appColor)),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text('عدد النقاط :', style: TextStyle(color: appColor)),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text('حالة الحساب :', style: TextStyle(color: appColor)),

                ],
              ),
              SizedBox(width: 5.0,),

              //results column
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    _user.name,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(_user.mobile, style: TextStyle(fontWeight: FontWeight.bold)),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(_user.email, style: TextStyle(fontWeight: FontWeight.bold)),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(_user.birthdate == null ? 'غير متاح' : '${_user.birthdate}',

                      style: TextStyle(fontWeight: FontWeight.bold)),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text('${dateTime(_user.registerDate)}',
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text('${_user.points}',
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  SizedBox(
                    height: 20.0,
                  ),
                  Text(_user.isStopped ? 'حساب موقوف' : 'حساب نشط',
                      style: TextStyle(fontWeight: FontWeight.bold)),
                ],
              ),
            ],
          ),
          SizedBox(height: 15.0,),
          InkWell(
            onTap: (){

              Navigator.push(context, MaterialPageRoute(builder: (BuildContext context)=>Registeration(_user)));

            },
            child: Text("تعديل الحساب",style: TextStyle(decoration: TextDecoration.underline,color: appColor,fontSize: 11.0),),
          )
        ],
      ),
    );
  }
}
