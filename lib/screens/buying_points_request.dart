import 'package:flutter/material.dart';
import 'package:restu_ratingflutter_app/utils/statics.dart';

import 'package:restu_ratingflutter_app/utils/styles.dart' as styles;
import 'package:restu_ratingflutter_app/utils/colors.dart';

import 'package:restu_ratingflutter_app/reusable_widgets/image.dart';

import 'package:restu_ratingflutter_app/models/resturant.dart';
import 'package:restu_ratingflutter_app/models/buying_client_request.dart';

import 'package:restu_ratingflutter_app/screens/all_resto.dart';
import 'package:toast/toast.dart';

class BuyingPointsRequest extends StatefulWidget {
  @override
  _BuyingPointsRequestState createState() => _BuyingPointsRequestState();
}

class _BuyingPointsRequestState extends State<BuyingPointsRequest> {

  bool sendingData = false;
  Restaurant _selectedRestaurant;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print('+_+_+_++++++_++============= user_id $user_id ');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'رفع فاتورة الشراء',
            style: styles.appBarTextStyle,
          ),
        ),
        body: Container(
          height: double.infinity,
//            width: double.infinity,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bg_dark.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(18.0),
            child: Stack(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Container(
                      width: double.infinity,
                      decoration: BoxDecoration(
                          border: Border.all(color: appColor)),
                      child: Padding(
                        padding: const EdgeInsets.only(right:8.0),
                        child: SizedBox(
                          width: double.infinity,
                          child: DropdownButton<Restaurant>(
                            iconSize: 24,
                            elevation: 16,
                            isExpanded: true,
                            style: TextStyle(color: appColor),
//                                      underline: Container(
//                                        width: double.infinity,
//                                        padding: EdgeInsets.all(20.0),
//                                        height: 0,
//                                        color: appColor,
//                                      ),
                            hint: Text(
                              'اختر مطعم',
                              style: TextStyle(
                                  fontSize: 18.0,
                                  fontFamily: "CAREEM"),
                            ), // Not necessary for Option 1
                            value: _selectedRestaurant,
                            onChanged: (newValue) {
                              setState(() {
                                _selectedRestaurant = newValue;
                              });
                            },
                            items: allRestaus
                                .map<DropdownMenuItem<Restaurant>>(
                                    (Restaurant value) {
                                  return DropdownMenuItem<Restaurant>(
                                    value: value,
                                    child: Text(
                                      value.name,
                                      style: TextStyle(
                                          fontSize: 16.0,
                                          fontFamily: "CAREEM"),
                                    ),
                                  );
                                }).toList(),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 10.0,),
                    SizedBox(
                      width: double.infinity,
                      child: FlatButton(
                        padding: EdgeInsets.all(12.0),
                        onPressed: () {

                          if(_selectedRestaurant == null){
                            Toast.show('يرجي اختيار احد المطاعم', context);
                            return;
                          }

                          sendingData = true;
                          setState(() {});

                          BuyingClientRequestDao dao = BuyingClientRequestDao();
                          dao.sendBuyingPointsRequest(user_id, _selectedRestaurant.id, (){
                            sendingData = false;
                            Toast.show('تم إرسال طلبك .. لكم جزيل الشكر ', context);
                            setState(() {

                            });
                          });

                        },
                        child: Text(
                          'ارسال الفاتورة',
                          style: TextStyle(color: Colors.white),
                        ),
                        color: appColor,
                      ),
                    )
                  ],
                ),
                sendingData
                    ? Container(
                  width: double.infinity,
                  height: double.infinity,
                  color: Colors.black.withOpacity(0.4),
                  child: Center(
                    child: activityIndicator(),
                  ),
                )
                    : Container()
              ],
            ),
          ),
        ));
  }
}
