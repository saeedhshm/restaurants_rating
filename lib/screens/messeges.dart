import 'package:flutter/material.dart';

import 'package:restu_ratingflutter_app/models/point.dart';
import 'package:restu_ratingflutter_app/reusable_widgets/app_scaffold.dart';
import 'package:restu_ratingflutter_app/utils/colors.dart';

class PointMessages extends StatefulWidget {
  @override
  _PointMessagesState createState() => _PointMessagesState();
}

class _PointMessagesState extends State<PointMessages> {

  List<Point> points;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    ReplacingPoints points = ReplacingPoints();
    points.replacePoints((points){
      this.points = points;
      setState(() {

      });
    });
  }
  @override
  Widget build(BuildContext context) {

    return AppScaffold(context,
      screenTitle: 'النقاط المبدلة',
      bg: 'assets/images/bg_dark.png',
      container: Container(
        child: points == null ? Center(
          child: CircularProgressIndicator() ,
        ) : Container(
          padding: EdgeInsets.all(20.0),
           child: ListView.builder(
             itemCount: points.length,
               itemBuilder: (context,index){
                return Card(
                  child: Container(
                    padding: EdgeInsets.all(15.0),
                    child: Column(
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
//                            Text('اسم المطعم',style: TextStyle(color:appColor, ),),
//                            SizedBox(width: 8.0,),
                            Text(points[index].restauName,style: TextStyle(color: appColor,fontSize: 20.0,fontWeight: FontWeight.bold),)
                          ],
                        ),
                        SizedBox(height: 10.0,),
                        Row(
                          children: <Widget>[
                            Text('النقاط',style: TextStyle(color:appColor, ),),
                            SizedBox(width: 8.0,),
                            Text('${points[index].points}')
                          ],
                        ),
                        SizedBox(height: 10.0,),
                        Row(
                          children: <Widget>[
                            Text('الهدية',style: TextStyle(color:appColor, ),),
                            SizedBox(width: 8.0,),
                            Text(points[index].prize)
                          ],
                        ),
                        SizedBox(height: 10.0,),
                        Row(
                          children: <Widget>[
                            Text('رسالة',style: TextStyle(color:appColor ),),
                            SizedBox(width: 8.0,),
                            Expanded(child: Text(points[index].msgs))
                          ],
                        ),
                        SizedBox(height: 10.0,),
                      ],
                    ),
                  ),
                );
               }
           ),
        ),
      ),
    );
  }
}
